import System;
import System.Text;
import System.IO;
import System.Windows.Forms;
import Sony.Vegas;

var dt = new Timecode(0.2 * 1000);

function alert(txt)
{
	MessageBox.Show(txt);
}

function Clone(track)
{
	var ev = track.Events[0];
	var ev2 = undefined;
	if(ev.MediaType == MediaType.Video)
		ev2 = new VideoEvent(ev.Start, ev.Length);
	if(ev.MediaType == MediaType.Audio)
		ev2 = new AudioEvent(ev.Start, ev.Length);
	if(ev2 == undefined)
		return;
	track.Events.Add(ev2);
	
	for(var tk in ev.Takes)
	{
		var fle = new Media(tk.MediaPath);
		for(var stream in fle.Streams)
		{
			if(stream.MediaType != ev.MediaType)
				continue;
			var tk2 = new Take(stream, tk.IsActive);
			ev2.Takes.Add(tk2);
			tk2.Offset = tk.Offset;
			break;
		}
	}
	return ev2;
}

function Split(mas)
{
	var tracks = Vegas.Project.Tracks;
	for(var track in tracks)
	{
		if(!track.Selected)
			continue;
		var pos = track.Events[0].Start + track.Events[0].Length;
		var ev = undefined;
		var ev2;
		for(var i = 0; i < mas.length; i++)
			if(mas[i] == undefined)
			{
				pos = pos + dt;
			} else {
				if(ev != undefined)
				{
					ev2 = ev.Split(pos - ev.Start);
					track.Events.Remove(ev2);
				}
				ev2 = Clone(track);
				ev = ev2.Split(mas[i]);
				track.Events.Remove(ev2);
				ev.Start = pos;
				pos = pos + dt;
			}
	}
}

function getText() // Get pattern from user
{
	var form =new Form();
	form.Height = 400;
	form.Width = 210;
	form.Text = "INPUT";
	
	var lbl = new Label();
	lbl.Text = "Enter the pattern:";
	lbl.Parent = form;
	
	var txt = new TextBox();
	txt.Multiline = true;
	txt.AutoSize = false;
	txt.Width = 200;
	txt.Height = 300;
	txt.Top = 25;
	txt.Parent = form;
	
	var btn = new Button();
	btn.Text = "OK";
	btn.Top = 25 + 300 + 5;
	btn.Parent = form;
	btn.DialogResult = 1;
	
	form.ShowDialog();
	
	if(form.DialogResult != 1)
		return "";
	
	return txt.Text;
}

function pat2arr(txt)
{
	if(txt == "")
		return [];
	var mas = txt.split("\n");
	for(var i in mas)
	{
		if(mas[i] == "")
			continue;
		mas[i] = +mas[i].split("<>")[1];
		if(mas[i] != mas[i])
			mas[i] = undefined;
		else
			mas[i] = new Timecode(Math.floor(mas[i]*1000));
	}
	return mas;
}


Split(pat2arr(getText()));