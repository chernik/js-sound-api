﻿window.loadSoundCTX = function(){
	var w = window;
	w.SoundCTX = (function(){
// ----------------------------------------------------------------------------- Пресеты
		var patcherSamples = [
			["Generator", '{"obj":{"glb":{"sizeSQ":30},"loc":{"arr":[{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Cinners":1},"loc":{}},"back":{"obj":{"glb":{"caption":"Sound join","posX":0,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"joinM"},"name":"joinS"},"state":{"isHide":true,"coor":[469,27]},"connected":[]},{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)"]}},"back":{"obj":{"glb":{"caption":"VISUAL","posX":617,"posY":9,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"visual"},"state":{"isHide":false,"coor":[465,76]},"connected":[{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Cinners":10},"loc":{}},"back":{"obj":{"glb":{"caption":"Sound join","posX":133,"posY":121,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"joinM"},"name":"joinS"},"state":{"isHide":true,"coor":[467,127]},"connected":[null,{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{"gz":220},"loc":{}},"back":{"obj":{"glb":{"caption":"GENER","posX":939,"posY":17,"UPcolor":"33FF33"},"loc":{}},"back":{},"name":"window"},"name":"generator"},"state":{"isHide":false,"coor":[414,125]},"connected":[null,null,{"type":"S","inner":4,"outer":0}]},{"obj":{"obj":{"glb":{"Rpoint":5,"tableX":0,"tableY":0},"loc":{"points":[[0,0],[0.6666666666666667,-0.49333333333333407],[0.8066666666666666,0.3866666666666667]]}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)","rgb(255, 100, 100)"]}},"back":{"obj":{"glb":{"caption":"AUTO","posX":618,"posY":197,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"auto"},"state":{"isHide":false,"coor":[367,98]},"connected":[null,null,null,{"type":"F","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{"Rpoint":5,"tableX":0,"tableY":0},"loc":{"points":[[0.006666666666666821,-0.6533333333333333],[0.4866666666666667,0.013333333333333308]]}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)","rgb(255, 100, 100)"]}},"back":{"obj":{"glb":{"caption":"AUTO","posX":941,"posY":108,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"auto"},"state":{"isHide":false,"coor":[309,96]},"connected":[null,null,null,null,{"type":"F","inner":4,"outer":0}]},{"obj":{"obj":{"glb":{"Rpoint":5,"tableX":16,"tableY":12},"loc":{"points":[[0,0.5833333333333334],[0.375,0.5833333333333334],[0.375,0],[0.5625,0],[0.6875,0.5833333333333334]]}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)","rgb(255, 100, 100)"]}},"back":{"obj":{"glb":{"caption":"AUTO","posX":618,"posY":384,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"auto"},"state":{"isHide":false,"coor":[364,158]},"connected":[null,null,null,{"type":"F","inner":1,"outer":0}]}]}},"back":{"obj":{"glb":{"Xsize":500,"Ysize":500},"loc":{"styles":["rgb(0,0,0)","rgb(255,0,0)"]}},"back":{"obj":{"glb":{"caption":"Patcher","posX":100,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"patcher"}'],
			["Recorder", '{"obj":{"glb":{"sizeSQ":30},"loc":{"arr":[{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Cinners":1},"loc":{}},"back":{"obj":{"glb":{"caption":"Sound join","posX":0,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"joinM"},"name":"joinS"},"state":{"isHide":true,"coor":[465,31]},"connected":[null,null,null,null,null]},null,{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"caption":"Recorder","posX":631,"posY":214,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"recorder"},"state":{"isHide":false,"coor":[362,32]},"connected":[null,null,null,null,{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"caption":"INNER","posX":635,"posY":338,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"input"},"state":{"isHide":false,"coor":[308,33]},"connected":[null,null,{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)"]}},"back":{"obj":{"glb":{"caption":"SPECTR","posX":630,"posY":12,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"spectr"},"state":{"isHide":false,"coor":[409,30]},"connected":[{"type":"S","inner":0,"outer":0}]}]}},"back":{"obj":{"glb":{"Xsize":500,"Ysize":500},"loc":{"styles":["rgb(0,0,0)","rgb(255,0,0)"]}},"back":{"obj":{"glb":{"caption":"Patcher","posX":100,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"patcher"}'],
			["Cutter", '{"obj":{"glb":{"sizeSQ":30},"loc":{"arr":[{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Cinners":1},"loc":{}},"back":{"obj":{"glb":{"caption":"Sound join","posX":0,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"joinM"},"name":"joinS"},"state":{"isHide":true,"coor":[466,38]},"connected":[]},{"obj":{"obj":{"glb":{"VXsize":320,"VYsize":240},"loc":{"arrayB":null}},"back":{"obj":{"glb":{"caption":"Player: ~warn~","posX":646,"posY":35,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"video"},"state":{"isHide":false,"coor":[404,37]},"connected":[{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{"patDefTime":0.5,"colKey":"rgb(50,50,255)","listSize":10,"patPerBit":1},"loc":{"times":[["Pat #1",0],["Pat #2",0.5]],"patt":[["Pat #1",0],["Pat #1",0],["Pat #2",0.5],["Pat #1",0]]}},"back":{"obj":{"glb":{"caption":"Cutter","posX":625,"posY":259,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"cutter"},"state":{"isHide":false,"coor":[337,41]},"connected":[null,{"type":"E","inner":0,"outer":0}]}]}},"back":{"obj":{"glb":{"Xsize":500,"Ysize":500},"loc":{"styles":["rgb(0,0,0)","rgb(255,0,0)"]}},"back":{"obj":{"glb":{"caption":"Patcher","posX":100,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"patcher"}'],
			["Volume & Pan", '{"obj":{"glb":{"sizeSQ":30},"loc":{"arr":[{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Cinners":2},"loc":{}},"back":{"obj":{"glb":{"caption":"Sound join","posX":0,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"joinM"},"name":"joinS"},"state":{"isHide":true,"coor":[74,22]},"connected":[]},{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"caption":"Volumer & Panner","posX":279,"posY":226,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"volpan"},"state":{"isHide":true,"coor":[34,117]},"connected":[null,null,{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{},"loc":{}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)"]}},"back":{"obj":{"glb":{"caption":"VISUAL","posX":609,"posY":37,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"visual"},"state":{"isHide":false,"coor":[25,66]},"connected":[{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{"detune":0,"snd":{"type":"wat","obj":{}}},"loc":{}},"back":{"obj":{"glb":{"caption":"FILE: \'... — копия.mp3\'","posX":615,"posY":236,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"file"},"state":{"isHide":false,"coor":[35,177]},"connected":[null,{"type":"S","inner":0,"outer":0}]},{"obj":{"obj":{"glb":{"Rpoint":5,"tableX":0,"tableY":0},"loc":{"points":[[0.043333333333333446,-0.9066666666666667],[0.48666666666666664,-0.17333333333333334]]}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)","rgb(255, 100, 100)"]}},"back":{"obj":{"glb":{"caption":"AUTO","posX":615,"posY":363,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"auto"},"state":{"isHide":false,"coor":[210,85]},"connected":[null,{"type":"F","inner":1,"outer":0}]},{"obj":{"obj":{"glb":{"Rpoint":5,"tableX":0,"tableY":0},"loc":{"points":[[0.0033333333333332993,0.7599999999999998],[0.44666666666666666,-0.8400000000000001]]}},"back":{"obj":{"glb":{"Xsize":300,"Ysize":150},"loc":{"styles":["rgb(0, 0, 0)","rgb(255, 100, 100)"]}},"back":{"obj":{"glb":{"caption":"AUTO","posX":174,"posY":393,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"auto"},"state":{"isHide":false,"coor":[209,138]},"connected":[null,{"type":"F","inner":2,"outer":0}]}]}},"back":{"obj":{"glb":{"Xsize":500,"Ysize":500},"loc":{"styles":["rgb(0,0,0)","rgb(255,0,0)"]}},"back":{"obj":{"glb":{"caption":"Patcher","posX":100,"posY":0,"UPcolor":"CCCCCC"},"loc":{}},"back":{},"name":"window"},"name":"graph"},"name":"patcher"}']
		];
// --------------------------------------------------------------------------------- Вспомогательные функции
		function newEl(par, tag){ // add GUI element
			var a = w.document.createElement(tag);
			if(par != null){
				par.appendChild(a);
			}
			a.style.display = "block";
			return a;
		}
		function findCoor(el){ // Координаты объекта
			if(!el.offsetParent)
				return [0, 0];
			var Acoor = [
				el.offsetLeft - el.scrollLeft,
				el.offsetTop  - el.scrollTop
			];
			var Bcoor = findCoor(el.offsetParent);
			return [
				Acoor[0] + Bcoor[0],
				Acoor[1] + Bcoor[1]
			];
		}
		function antiTrunc(v){ // Дробная часть
			return v - Math.floor(v);
		}
		function filterObj(inp){ // Удаление полей undefined
			var outp = {};
			var i;
			for(i in inp){
				//alert(i + " " + (inp[i] !== undefined));
				if(inp[i] !== undefined)
					outp[i] = inp[i];
			}
			return outp;
		}
		function newRange(cont, min, max, val, fMain, fDesc){ // Бегунок
			if(fMain === undefined)
				fMain = null;
			if(fDesc === undefined)
				fDesc = null;
			if(fMain === null)
				throw "fMain must exist!";
			var div = newEl(cont, "div");
			var inp = newEl(div, "input");
				inp.type = "range";
				inp.value = val;
				inp.min = min;
				inp.max = max;
			var desc = null;
			if(fDesc !== null){
				desc = newEl(div, "span");
				desc.style.textSize = "15px";
			}
			function re(v){
				if(v === undefined)
					v = +inp.value;
				inp.value = v;
				if(desc !== null)
					desc.innerHTML = fDesc(v);
				return fMain(v);
			}
			inp.onmousedown = function(e){
				inp.onchange = function(e){
					re();
					obj.log();
				}
				w.onmousemove = function(e){
					re();
				}
				w.onmouseup = function(e){
					obj.log();
					w.onmousemove = null;
					w.onmouseup = null;
				}
			}
			re(val);
			return [div, re];
		}
		function newFileLoad(div, evs){ // Загрузчик файлов
			var loc = {};
			loc.formFile = newEl(div, "input");
			loc.formFile.type = "file";
			loc.formFile.style.display = "none";
			loc.formFile.onchange = function(e){
				var arr = this.files;
				if(evs(arr))
					return;
			}
			return loc.formFile;
		}
		function JSONencode(obj){ // Преобразование для нестроковых объектов
			if(obj == null)
				return {
					type: "simple",
					obj: obj
				};
			return {
				type: "wat",
				obj: obj
			};
		}
		function JSONdecode(obj){
			switch(obj.type){
			case "simple":
				return obj.obj;
			case "wat":
				return null;
			}
		}
		var menuDiv = newEl(w.document.body, "div"); // Всплывающее меню
			menuDiv.style.display = "none";
			menuDiv.style.position = "absolute";
			menuDiv.style.zIndex = "10";
			menuDiv.oncontextmenu = function(e){return false;}
		function newMenu(arr){
			var res = {};
			res.open = function(coor){
				menuDiv.innerHTML = "";
				var list = newEl(menuDiv, "select");
				list.size = arr.length;
				list.multiple = true;
				list.id = "menu";
				var str = "";
				var i;
				for(i = 0; i < arr.length; i++)
					str += '<option>' + arr[i][0] + '</option>';
				list.innerHTML = str;
				menuDiv.style.display = "block";
				if(coor != undefined){
					menuDiv.style.left = coor[0] + "px";
					menuDiv.style.top = coor[1] + "px";
				}
				w.onclick = function(e){
					menuDiv.style.display = "none";
					w.onclick = null;
					if(e.target.parentElement.id != "menu" && e.target.id != "menu")
						return;
					var ind = list.selectedIndex;
					if(ind >= 0)
						arr[ind][1]();
				}
			}
			if(arr === undefined){
				res.open = function(coor){};
			}
			return res;
		}
		function menuInput(type, str, def){
			var res;
			switch(type){
			case "color":
			case "string":
			case "number":
				res = prompt(str, def);
				if(res == null)
					return null;
				if(type == "string" || type == "color") // Пока
					return res;
				res = +res;
				if(res != res || res == Infinity || res == -Infinity)
					return null;
				return res;
			}
			return null;
		}
		/*
		sp - SoundParam
		inner - innerS
		tmr - time for setInterval
		f - function inner => sp
		pars.bpc - BeatsPerCircle
		pars.n - count of parts of circle
		*/
		function drvAudioParam(sp, inner, tmr, f, pars){ // Связь AudioParam и innerF
			var loc = {};
			var glb = {};
			loc.timer = loc.arr = null;
			if(f === undefined){
				f = function(v){return v;}
			}
			loc.tik = function(){
				var t = obj.ctx.currentTime;
				var k = 60 * pars.bpc / obj.bpm; // Sec Per Circle
				var pos = Math.floor(t / k) * k;
				var i;
				var n = loc.arr.length-1;
				for(i = 0; i <= n; i++){
					loc.arr[i] = f(inner.get(i/n, pos + i/n * k));
				}
				var err;
				try{
					//sp.setValueCurveAtTime(loc.arr, pos, k - 0.01);
				}catch(err){}
				try{
					sp.setValueCurveAtTime(loc.arr, pos + k, k - 0.01);
				}catch(err){};
			}
			glb.start = function(){
				loc.arr = new Float32Array(pars.n+1);
				loc.timer = w.setInterval(loc.tik, tmr);
			}
			glb.stop = function(){
				w.clearInterval(loc.timer);
			}
			return glb;
		}
		function newCircleRange(cont, changeEvent){ // where and what on change
			if(changeEvent === undefined || changeEvent === null){
				changeEvent = function(){};
			}
			var loc = {};
			loc.gr = newEl(cont, "canvas").getContext("2d");
			loc.gr.canvas.width  = 30;
			loc.gr.canvas.height = 30;
			loc.angle = 0;
			loc.grAngle = function(){ // angle -> real angle
				return Math.PI * (5 * loc.angle - 4) / 3;
			}
			loc.chAngle = function(d){ // change angle
				loc.angle += d * 0.01;
				if(loc.angle > 1){
					loc.angle = 1;
				}
				if(loc.angle < 0){
					loc.angle = 0;
				}
			}
			loc.out = function(){ // on change
				changeEvent(loc.angle);
			}
			loc.grOut = function(){ // draw range
				loc.gr.canvas.width += 0;
				loc.gr.beginPath();
				loc.gr.moveTo(15,15);
				loc.gr.ellipse(15,15,15,14,loc.grAngle(),0,Math.PI*2);
				loc.gr.lineTo(15,15);
				loc.gr.strokeStyle = "black";
				loc.gr.stroke();
			}
			loc.gr.canvas.onmousedown = function(e){ // moveing
				var mPos = [e.clientX, e.clientY];
				w.onmouseup = function(e){
					w.onmousemove = null;
				}
				w.onmousemove = function(e){
					var mNewPos = [e.clientX, e.clientY];
					var dPos = [
						mNewPos[0] - mPos[0],
						mNewPos[1] - mPos[1]
					];
					mPos = mNewPos;
					loc.chAngle(dPos[0]); // По X двигаем мышью
					loc.out();
					loc.grOut();
				}
			}
			loc.value = function(v){
				if(v !== undefined){
					loc.angle = v;
					loc.out();
					loc.grOut();
				}
				return loc.angle;
			}
			return {
				target: loc.gr.canvas,
				value: loc.value
			};
		}
// ------------------------------------------------------------------------ SoundOBJ
		var obj = {};
		try{
			obj.ctx = w.AudioContext || w.webkitAudioContext;
			obj.ctx = new obj.ctx();
		}catch(err){
			obj.ctx = null;
		}
		if(obj.ctx == null)
			throw "NO SOUND";
		obj.bpm = 60;
		obj.startPos = obj.ctx.currentTime;
		obj.target = w.document.body;
 // ----------------------------------------------------------------------- Больше вспом функций
		var swapTarget = function(th){
			var buf = obj.target;
			obj.target = th;
			return buf;
		}
		function norm(a){
			a.style.display = "";
			return a;
		}
		function cutStr(str, k){
			if(str.length <= k)
				return str;
			else
				return "..." + str.slice(str.length - k + 3);
		}
		obj.objs = {};
		obj.validObjs = {
			"Audio file": "file",
			"Visual wave": "visual",
			"Automation": "auto",
			"Generator": "generator",
			"Sound joiner": "joinS",
			"Saper)": "saperCall",
			"Video file": "video",
			"Microphone input": "input",
			"Patcher": "patcher",
			"Recorder": "recorder",
			"Visual spector": "spectr",
			"Drum partition": "drums",
			"Volume & Panner": "volpan",
			"Tester)": "tester",
			"Event joiner": "joinE",
			"Test: cutter": "cutter",
			"Test: Drum Machine": "drummachine",
			"Test: EQ": "eq",
			"Test: timeMarker": "timeMarker"
		};
		// ------------------------------------------------------------------------------------------- INNER_M & OUTER_M
		obj.objs.innerM = function(){
			var glb = {};
			glb.type = "M";
			glb.isConnect = false;
			glb.outer = null;
			return glb;
		}
		obj.objs.outerM = function(con, discon){
			var glb = {};
			var loc = {};
			glb.type = "M";
			loc.catchers = [];
			glb.disconnect = function(inner){
				if(inner.type != glb.type)
					throw "different types";
				var ind = loc.catchers.findIndex(function(e){return e == inner;});
				if(ind == -1)
					throw "cannot disconnect thing which no connect";
				loc.catchers = loc.catchers.slice(0, ind).concat(loc.catchers.slice(ind+1, loc.catchers.length));
				discon(loc, inner);
				inner.isConnect = false;
				inner.outer = null;
			}
			glb.disAll = function(){
				var i;
				for(i = 0; i < loc.catchers.length; i++)
					glb.disconnect(loc.catchers[i]);
			}
			glb.connect = function(inner){
				if(inner.type != glb.type)
					throw "different types";
				loc.catchers.push(inner);
				if(inner.isConnect)
					throw "Inner is already connect";
				con(loc, inner);
				inner.isConnect = true;
				inner.outer = glb;
			}
			return glb;
		}
		// ------------------------------------------------------------------------------------------- INNER_S & OUTER_S
		obj.objs.innerS = function(ctx){
			var glb = obj.objs.innerM();
			glb.ctx = ctx;
			glb.type = "S";
			return glb;
		}
		obj.objs.outerS = function(ctx){
			var glb = obj.objs.outerM(
				function(loc, inner){
					ctx.connect(inner.ctx);
				},
				function(loc, inner){
					ctx.disconnect(inner.ctx);
				}
			);
			glb.type = "S";
			return glb;
		}
		// ------------------------------------------------------------------------------------------- INNER_F & OUTER_F
		obj.objs.innerF = function(){
			var glb = obj.objs.innerM();
			glb.f = null;
			glb.type = "F";
			glb.get = function(v, t){
				if(glb.f == null)
					return 0;
				else
					return glb.f(v, t);
			}
			return glb;
		}
		obj.objs.outerF = function(f){
			var glb = obj.objs.outerM(
				function(loc, inner){
					inner.f = f;
				},
				function(loc, inner){
					inner.f = null;
				}
			);
			glb.type = "F";
			return glb;
		}
		// ------------------------------------------------------------------------------------------ INNER_E & OUTER_E
		obj.objs.innerE = function(f){
			var glb = obj.objs.innerM();
			glb.f = f;
			glb.type = "E";
			return glb;
		}
		obj.objs.outerE = function(){
			var loc = {};
			loc.f = null;
			var glb = obj.objs.outerM(
				function(locc, inner){
					loc.f = inner.f;
				},
				function(loc, inner){
					loc.f = null;
				}
			);
			glb.type = "E";
			glb.run = function(e){
				if(e == undefined)
					e = {};
				if(loc.f != null)
					loc.f(e);
			}
			return glb;
		}
		// ------------------------------------------------------------------------------------------------------ WINDOW
		obj.objs.window = function(){
			var loc = {}; // Приватные объекты
			var glb = {}; // Публичные объекты
			// GUI out
			loc.div = newEl(obj.target, "div");
			loc.div.style.position = "absolute";
				loc.pos = [];
				glb.setPosX = function(v){loc.pos[0] = v; loc.div.style.left = v + "px";}
				glb.setPosY = function(v){loc.pos[1] = v; loc.div.style.top  = v + "px";}
				glb.getPosX = function(){return loc.pos[0];}
				glb.getPosY = function(){return loc.pos[1];}
			glb.setPosX(0); glb.setPosY(0);
			loc.div.style.border = "1px solid black"
			loc.div.style.padding = "2px";
			loc.divUP = newEl(loc.div, "div");
			loc.divSH = newEl(loc.divUP, "button");
				loc.divSH.style.display = "inline";
				loc.divSH.style.border = "1px solid black";
				loc.divSH.style.padding = "2px";
				loc.divSH.style.textAlign = "center";
				loc.divSH.style.cursor = "default";
				loc.divSHstate = true;
				glb.setWState = function(fl){
					loc.divSHstate = fl;
					if(fl == null){
						loc.div.style.display = "none";
						return;
					} else
						loc.div.style.display = "block";
					if(loc.divSHstate){
						loc.divDOWN.style.display = "block";
						loc.divSH.innerHTML = "^";
					} else {
						loc.divDOWN.style.display = "none";
						loc.divSH.innerHTML = "V";
					}
				}
				loc.divSH.onclick = function(e){
					glb.setWState(!loc.divSHstate);
				}
				loc.divSH.innerHTML = "^";
			loc.divCAPT = newEl(loc.divUP, "span");
				loc.divCAPT.style.display = "inline";
				//loc.divCAPT.style.border = "1px solid black";
				loc.divCAPT.style.padding = "2px";
				loc.divCAPT.style.textAlign = "center";
				loc.divCAPT.style.width = "100%";
			loc.caption = "";
				glb.setCaption = function(v){loc.caption = v; loc.divCAPT.innerHTML = v;}
				glb.getCaption = function(){return loc.caption;}
			glb.setCaption("WINDOW");
				loc.divUPcolor = "";
				glb.setUPcolor = function(v){loc.divUPcolor = v; loc.divUP.style.backgroundColor = v;}
				glb.getUPcolor = function(){return loc.divUPcolor;}
			glb.setUPcolor("CCCCCC");
			loc.divUP.style.textAlign = "center";
			loc.divUP.style.cursor = "move";
			loc.divDOWN = newEl(loc.div, "div");
				glb.getContainer = function(){return loc.divDOWN;}
			// GUI in
			loc.divUP.onmousedown = function(e){
				if(e.button != 0)
					return;
				var s = [e.clientX, e.clientY];
				var f = s;
				w.onmousemove = function(e){
					f = [e.clientX, e.clientY];
					glb.setPosX(glb.getPosX() + f[0] - s[0]);
					glb.setPosY(glb.getPosY() + f[1] - s[1]);
					s = f;
				}
				w.onmouseup = function(e){
					w.onmousemove = null;
					w.onmouseup = null;
				}
			}
			// Saver
			loc.setCaption = glb.getCaption;
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.caption = glb.getCaption();
				res.obj.glb.posX = glb.getPosX();
				res.obj.glb.posY = glb.getPosY();
				res.obj.glb.UPcolor = glb.getUPcolor();
				res.obj.loc = {};
				res.back = {};
				res.name = "window";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "window")
					throw "Wrong type state";
				loc.setCaption(res.obj.glb.caption);
				glb.setPosX(res.obj.glb.posX);
				glb.setPosY(res.obj.glb.posY);
				glb.setUPcolor(res.obj.glb.UPcolor);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return glb;
		}
		// --------------------------------------------------------------------------------------------------------- FILE
		obj.objs.file = function(){
			var glb = obj.objs.window();
			var loc = {};
			// change methods
			loc.setCaptionB = glb.setCaption; // Caption
				glb.setCaption = function(v){loc.caption = v; loc.setCaptionB("LOCAL: '" + v + "'");}
				loc.caption = "";
				loc.setCaption = function(v){loc.caption = v; loc.setCaptionB("FILE: '" + cutStr(v, 15) + "'");}
				//glb.getCaption = function(){return loc.caption;}
			loc.setCaption("~none~");
			loc.div = glb.getContainer(); // Container
				glb.getContainer = undefined;
			// system
			loc.formFile = newFileLoad(loc.div, function(arr){
				loc.SH("load");
				loc.canStart = false;
				loc.setCaption("~opening~");
				if(arr.length == 0){
					loc.canStart = true;
					loc.SH("start");
					loc.setCaption("~empty~");
					return;
				}
				loc.setCaption(arr[0].name);
				var f = new FileReader();
				f.onload = function(e){
					obj.ctx.decodeAudioData(e.target.result)
						.then(function(e){
							loc.SH("start");
							glb.setSnd(e);
						}).catch(function(e){
							loc.SH("start");
							loc.setCaption("~error~");
							console.error(e);
						});
				}
				f.readAsArrayBuffer(arr[0]);
			});
			loc.ctx = obj.ctx.createGain(); // context
			loc.ctxBuf = null; // real context
			loc.snd = null; // sound
				glb.setSnd = function(v){loc.snd = v;}
				glb.getSnd = function(){return loc.snd;}
			loc.canStart = true;
			loc.loop = true;
			glb.setLoop = function(fl){
				loc.loop = fl;
			}
			glb.getLoop = function(){
				return loc.loop;
			}
			glb.start = function(s, f){
				if(loc.ctxBuf != null)
					glb.stop();
				loc.ctxBuf = obj.ctx.createBufferSource();
				loc.ctxBuf.buffer = loc.snd;
				loc.ctxBuf.connect(loc.ctx);
				loc.ctxBuf.loop = loc.loop;
				loc.ctxBuf.detune.setValueAtTime(loc.detune * 100, obj.ctx.currentTime + 0.1);
				loc.ctxBuf.start(0, s === undefined ? 0 : s);
				if(s !== undefined)
					loc.ctxBuf.loopStart = s;
				if(f !== undefined)
					loc.ctxBuf.loopEnd = f;
			}
			glb.stop = function(){
				if(loc.ctxBuf === null)
					return;
				loc.ctxBuf.stop();
				loc.ctxBuf.disconnect(loc.ctx);
				loc.ctxBuf = null;
			}
			loc.detune = 0;
				glb.setDetune = function(v){
					v = +v;
					if(v != v)
						throw "Wrong detune value";
					loc.detune = v;
					if(loc.ctxBuf != null)
						loc.ctxBuf.detune.setValueAtTime(v * 100, obj.ctx.currentTime + 0.1);
				}
				glb.getDetune = function(){return loc.detune;}
			// GUI out
			loc.butLoad = newEl(loc.div, "button");
			loc.butLoad.innerHTML = "LOAD";
			loc.butStart = newEl(loc.div, "button");
			loc.butStart.innerHTML = "START";
			loc.butStop = newEl(loc.div, "button");
			loc.butStop.innerHTML = "STOP";
			loc.SH = function(state){ // show / hide
				switch(state){
				case "stop":
					loc.butLoad .style.display = "block";
					loc.butStart.style.display = "none";
					loc.butStop .style.display = "block";
					break;
				case "start":
					loc.butLoad .style.display = "block";
					loc.butStart.style.display = "block";
					loc.butStop .style.display = "none";
					break;
				case "load":
					loc.butLoad .style.display = "block";
					loc.butStart.style.display = "none";
					loc.butStop .style.display = "none";
					break;
				}
			}
				loc.SH("start");
			// GUI in
			loc.butLoad.onclick = function(e){
				loc.butStop.click();
				loc.formFile.click();
			}
			loc.butStart.onclick = function(e){
				glb.start();
				loc.SH("stop");
			}
			loc.butStop.onclick = function(e){
				glb.stop();
				loc.SH("start");
			}
			loc.rangeDetune = newRange(loc.div, -12, 12, 0,
				function(v){
					glb.setDetune(v);
					obj.log("note", v);
				},
				function(v){
					return v*100 + "% detune";
				}
			);
			// menu
			loc.menu = newMenu([
				["Loop?", function(){
					glb.setLoop(!!confirm("Loop?"));
				}]
			]);
			loc.div.oncontextmenu = function(e){
				loc.menu.open([e.clientX, e.clientY]);
				return false;
			}
			// inners & outers
			loc.inners = [obj.objs.innerE(function(e){
				if(e.t != undefined)
					glb.start(e.t);
				else
					glb.stop();
			})];
			glb.getInners = function(){return loc.inners;}
			loc.outers = [obj.objs.outerS(loc.ctx)];
			glb.getOuters = function(){return loc.outers;}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.detune = glb.getDetune();
				res.obj.glb.snd = JSONencode(glb.getSnd());
				res.obj.glb.loop = glb.getLoop();
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "file";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "file")
					throw "Wrong type state";
				glb.setDetune(res.obj.glb.detune);
					loc.rangeDetune[1](glb.getDetune());
				glb.setSnd(JSONdecode(res.obj.glb.snd));
				glb.setLoop(!!res.obj.glb.loop);
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// -------------------------------------------------------------------------------------------------- OUT
		obj.objs.out = function(){
			var glb = obj.objs.window();
			var loc = {};
			// change methods
			glb.setCaption("OUT"); // caption
				loc.setCaption = glb.setCaption;
				glb.setCaption = undefined;
			glb.setUPcolor("FFAAAA"); // color
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			// system
			loc.ctx = obj.ctx.createGain(); // Вход
			loc.ctx.connect(obj.ctx.destination); // Вывод
			// GUI out
			loc.butGlob = newEl(loc.div, "button");
				loc.butGlob.innerHTML = "SETTINGS";
			loc.log = newEl(loc.div, "div");
			loc.log.style.textSize = "15px";
			glb.setLog = function(v){
				loc.log.innerHTML = v;
			}
			// GUI in
			loc.menuGlob = newMenu([
				["BPM", function(){
					var res = menuInput("number",
						"Введите BPM:",
						obj.bpm);
					if(res != res)
						return;
					obj.bpm = res;
				}]
			]);
			loc.butGlob.onclick = function(e){
				w.setTimeout(function(){
					loc.menuGlob.open([e.clientX, e.clientY]);
				},10);
			}
			// inners
			loc.inners = [obj.objs.innerS(loc.ctx)];
			glb.getInners = function(){return loc.inners;}
			// outers
			loc.outers = [];
			glb.getOuters = function(){return loc.outers;}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "out";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "out")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- GRAPH
		obj.objs.graph = function(){
			var glb = obj.objs.window();
			var loc = {};
			// change methods
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			// GUI out
			loc.draw = function(){};
				glb.setDraw = function(f){loc.draw = f;}
			loc.gr = newEl(loc.div, "canvas").getContext("2d");
				glb.getGr = function(){return loc.gr;}
			loc.styles = ["rgb(0, 0, 0)"];
				glb.setStyle = function(ind, v){
					loc.styles[ind] = v;
					loc.draw();
				}
				glb.getStyle = function(ind){
					return loc.styles[ind];
				}
			loc.size = [0, 0];
				glb.setXsize = function(v){
					loc.size[0] = v;
					loc.gr.canvas.width = v;
					loc.draw();
				}
				glb.setYsize = function(v){
					loc.size[1] = v;
					loc.gr.canvas.height = v;
					loc.draw();
				}
				glb.getXsize = function(){return loc.size[0];}
				glb.getYsize = function(){return loc.size[1];}
				glb.setXsize(300);
				glb.setYsize(150);
			loc.timer = null;
			glb.start = function(){
				if(loc.timer != null)
					return;
				loc.timer = w.setInterval(function(){loc.draw();}, 10);
			}
			glb.stop = function(){
				if(loc.timer === null)
					return;
				w.clearInterval(loc.timer);
				loc.timer = null;
			}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.Xsize = glb.getXsize();
				res.obj.glb.Ysize = glb.getYsize();
				res.obj.loc = {};
				res.obj.loc.styles = loc.styles;
				res.back = loc.backSaver.getState();
				res.name = "graph";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "graph")
					throw "Wrong type state";
				glb.setXsize(res.obj.glb.Xsize);
				glb.setYsize(res.obj.glb.Ysize);
				loc.styles = res.obj.loc.styles;
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- VISUAL
		obj.objs.visual = function(){
			var glb = obj.objs.graph();
			var loc = {};
			glb.setCaption("VISUAL");
			// system
			loc.ctx = obj.ctx.createAnalyser();
			loc.ctx.fftSize = 1024; // len
			loc.max = 256; // low
			loc.len = loc.ctx.frequencyBinCount;
			loc.data = new Uint8Array(loc.len);
			// GUI out
			loc.gr = glb.getGr();
				glb.getGr = undefined;
			glb.setDraw(function(){
				loc.ctx.getByteTimeDomainData(loc.data);
				loc.gr.canvas.width += 0;
				loc.gr.strokeStyle = glb.getStyle(0);
				loc.gr.beginPath();
				var kx = function(x){
					return 1.0 * x * loc.gr.canvas.width / loc.len;
				}
				var ky = function(y){
					return 1.0 * loc.gr.canvas.height * (1 - y / loc.max);
				}
				var i;
				for(i = 0; i < loc.len; i++){
					if(i == 0)
						loc.gr.moveTo(kx(i), ky(loc.data[i]));
					else
						loc.gr.lineTo(kx(i), ky(loc.data[i]));
				}
				loc.gr.stroke();
			});
				loc.setDraw = glb.setDraw;
				glb.setDraw = undefined;
			// inners
			loc.inners = [obj.objs.innerS(loc.ctx)];
			glb.getInners = function(){return loc.inners;}
			// outers
			loc.outers = [obj.objs.outerS(loc.ctx)];
			glb.getOuters = function(){return loc.outers;}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "visual";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "visual")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			glb.start();
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- AUTO
		obj.objs.auto = function(){
			var glb = obj.objs.graph();
			var loc = {};
		// change methods
			glb.setCaption("AUTO");
		// inners
			loc.inners = [obj.objs.innerF()];
			glb.getInners = function(){
				return loc.inners;
			}
		// system
			loc.points = [[0,0]]; // Точки (произвольная позиция)
			loc.table = [0, 0]; // Сетка
				glb.setTableX = function(v){loc.table[0] = v; loc.filtAll();}
				glb.getTableX = function(){return loc.table[0];}
				glb.setTableY = function(v){loc.table[1] = v; loc.filtAll();}
				glb.getTableY = function(){return loc.table[1];}
			loc.filter = function(Rcoor){
				var coor = [Rcoor[0], Rcoor[1]];
				if(loc.table[0] > 0)
					coor[0] = Math.floor(coor[0] * loc.table[0]) / loc.table[0];
				if(loc.table[1] > 0)
					coor[1] = Math.floor(coor[1] * loc.table[1]) / loc.table[1];
				return coor;
			}
			loc.filtAll = function(){
				var i;
				for(i = 0; i < loc.points.length; i++)
					loc.points[i] = loc.filter(loc.points[i]);
			}
			loc.addPoint = function(coor){ // Добавить точку
				var x = coor[0];
				var y = coor[1];
				if(x < 0 || x >= 1 || y < -1 || y > 1)
					return;
				var ind = loc.points.findIndex(function(e){return e[0] > x}); // ищем первый превышающий элемент
				if(ind < 0)
					ind = loc.points.length;
				loc.points = loc.points.slice(0,ind). // Добавляем в массив
					concat([[x, y]]).
					concat(loc.points.slice(ind, loc.points.length));
				loc.inners.push(obj.objs.innerF()); // Добавляем слушатели
				loc.inners.push(obj.objs.innerF());
				return ind;
			}
			loc.remPoint = function(ind){ // Удалить точку по индексу
				if(ind >= loc.points.length || ind <= 0)
					return;
				loc.points = loc.points.slice(0, ind). // Удаляем из массива
					concat(loc.points.slice(ind+1, loc.points.length));
				loc.inners.pop(); // Удаляем слушатели
				loc.inners.pop();
			}
			loc.mathPoints = function(t, tm){ // critical time // Считаем реальную позицию точек от слушателей
				var arr = [];
				var i;
				for(i = 0; i < loc.points.length; i++){
					arr.push([
						loc.points[i][0],
						loc.points[i][1]
					]);
					arr[i] = loc.filter(arr[i]); // Пригоняем к сетке
					if(i == 0){
						arr[i][1] += loc.inners[0].get(t, tm);
					} else {
						arr[i][0] += loc.inners[-1+i*2+0].get(t, tm);
						arr[i][1] += loc.inners[-1+i*2+1].get(t, tm);
						if(arr[i][0] < arr[i-1][0])
							arr[i][0] = arr[i-1][0];
						if(arr[i][0] > 1)
							arr[i][0] = 1;
					}
					if(arr[i][1] > 1)  arr[i][1] = 1;
					if(arr[i][1] < -1) arr[i][1] = -1;
				}
				return arr;
			}
		// GUI out
			glb.setStyle(1, "rgb(255, 100, 100)");
			loc.guiPoints = function(points){
				var arr = [];
				function conv(pR){
					return [
						pR[0] * loc.gr.canvas.width / 1,
						loc.gr.canvas.height * (1 - pR[1]) / 2
					];
				}
				arr.push(conv([0, points[0][1]]));
				var i;
				for(i = 1; i < points.length; i++)
					arr.push(conv(points[i]));
				arr.push(conv([1, points[0][1]]));
				return arr;
			}
			loc.gr = glb.getGr();
				glb.getGr = undefined;
			loc.r = 5;
				glb.setRpoint = function(v){loc.r = v;}
				glb.getRpoint = function(){return loc.r;}
			glb.setDraw(function(){
				loc.gr.canvas.width += 0;
				function drawLine(arr, style){
					loc.gr.strokeStyle = style;
					loc.gr.beginPath();
					var i;
					for(i = 0; i < arr.length; i++)
						if(i == 0)
							loc.gr.moveTo(arr[i][0], arr[i][1]);
						else
							loc.gr.lineTo(arr[i][0], arr[i][1]);
					loc.gr.stroke();
					for(i = 0; i < arr.length; i++){
						loc.gr.beginPath();
						loc.gr.ellipse(arr[i][0], arr[i][1], loc.r, loc.r, 0, 0, Math.PI * 2, false);
						loc.gr.stroke();
					}
				}
				var arr = [];
				var i;
				for(i = 0; i < loc.points.length; i++) // Пригоняем к сетке
					arr[i] = loc.filter(loc.points[i]);
				drawLine(loc.guiPoints(arr), glb.getStyle(0));
				drawLine(
					loc.guiPoints(
						loc.mathPoints(
							antiTrunc(
								(obj.ctx.currentTime - obj.startPos) / 60 * obj.bpm // right
							),
							obj.ctx.currentTime
						)
					), glb.getStyle(1)
				);
			});
				glb.setDraw = undefined;
		// GUI in
			loc.menu = newMenu([
				["Радиус точек", function(){
					var res = menuInput("number",
						"Введите новый радиус управляющих точек",
						glb.getRpoint());
					if(res == null || res <= 0)
						return;
					glb.setRpoint(res);
				}],
				["Цвет основной линии", function(){
					var res = menuInput("color",
						"Выберите новый цвет основной линии",
						glb.getStyle(1));
					if(res == null)
						return;
					glb.setStyle(1, res);
				}],
				["Цвет анимированой линии", function(){
					var res = menuInput("color",
						"Выберите новый цвет анимированой линии",
						glb.getStyle(0));
					if(res == null)
						return;
					glb.setStyle(0, res);
				}],
				["Горизонтальная привязка", function(){
					var res = menuInput("number",
						"Введите количество позиций по горизонтали, \"0\" - для отсутствия привязки",
						glb.getTableX());
					if(res == null || res < 0)
						return;
					glb.setTableX(res);
				}],
				["Вертикальная привязка", function(){
					var res = menuInput("number",
						"Введите количество позиций по вертикали, \"0\" - для отсутствия привязки",
						glb.getTableY());
					if(res == null || res < 0)
						return;
					glb.setTableY(res);
				}]
			]);
			loc.getMpos = function(e){
				var Acoor = [e.clientX, e.clientY];
				var Bcoor = findCoor(e.target);
				return [
					Acoor[0] - Bcoor[0],
					Acoor[1] - Bcoor[1]
				];
			}
			loc.Dpoint = function(a, b){
				return Math.sqrt(
					Math.pow(a[0] - b[0], 2) +
					Math.pow(a[1] - b[1], 2));
			}
			loc.findPoint = function(coor){
				var arr = loc.guiPoints(loc.points);
				var ind = arr.findIndex(function(e){
					return loc.Dpoint(coor, e) <= loc.r;
				});
				if(ind == arr.length - 1)
					ind = 0;
				return ind;
			}
			loc.rewPoint = function(coor){
				return [
					coor[0] * 1 / loc.gr.canvas.width,
					1 - coor[1] * 2 / loc.gr.canvas.height
				];
			}
			loc.gr.canvas.oncontextmenu = function(e){
				return false;
			}
			loc.gr.canvas.onmousedown = function(e){
				var s = [e.clientX, e.clientY];
				var f = s;
				var pos = loc.getMpos(e);
				var ind = loc.findPoint(pos);
				var flMoar = false;
				if(ind >= 0 && e.button == 2){
					loc.remPoint(ind);
					return;
				}
				if(ind == -1){
					if(e.button == 2){
						w.onmouseup = function(e){
							loc.menu.open(s);
							w.onmouseup = null;
						}
						return;
					}
					var coor = loc.rewPoint(pos);
					ind = loc.addPoint(coor);
					/*if(e.button == 0) // Рисование точек не отрывая мыши, пока глючит
						flMoar = true;*/
				}
				w.onmousemove = function(e){
					f = [e.clientX, e.clientY];
					var fR = loc.rewPoint(f);
					if(flMoar){ // no trusted work
						var pos = loc.getMpos(e);
						var coor = loc.rewPoint(pos);
						ind = loc.addPoint(coor);
						return;
					}
					var sR = loc.rewPoint(s);
					var dR = [
						fR[0] - sR[0],
						fR[1] - sR[1]
					];
					loc.points[ind][0] += dR[0];
					if(ind > 0 && loc.points[ind-1][0] > loc.points[ind][0])
						loc.points[ind][0] = loc.points[ind-1][0];
					if(ind < loc.points.length - 1 && loc.points[ind+1][0] < loc.points[ind][0])
						loc.points[ind][0] = loc.points[ind+1][0];
					if(loc.points[ind][0] < 0)
						loc.points[ind][0] = 0;
					if(loc.points[ind][0] > 1)
						loc.points[ind][1] = 1;
					loc.points[ind][1] += dR[1];
					if(loc.points[ind][1] > 1)
						loc.points[ind][1] = 1;
					if(loc.points[ind][1] < -1)
						loc.points[ind][1] = -1;
					obj.log("point", [loc.filter(loc.points[ind]), loc.table]);
					s = f;
				}
				w.onmouseup = function(e){
					w.onmousemove = null;
					w.onmouseup = null;
					loc.points[ind] = loc.filter(loc.points[ind]);
					obj.log();
				}
			}
		// outers
			loc.outers = [obj.objs.outerF(function(v, t){ // critical time
				while(v < 0){
					v += 1;
				}
				while(v >= 1){
					v -= 1;
				}
				var points = loc.mathPoints(antiTrunc((t - obj.startPos) / 60 * obj.bpm), t);
				points[0][0] = 0;
				points.push([1, points[0][1]]);
				var ind = points.findIndex(function(e){return e[0] > v;});
				if(ind < 0)
					throw "OP";
				var Acoor = points[ind-1];
				var Bcoor = points[ind];
				var k = (Bcoor[1] - Acoor[1]) / (Bcoor[0] - Acoor[0]);
				return Acoor[1] + (v - Acoor[0]) * k;
			})];
			glb.getOuters = function(){return loc.outers;}
		// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.Rpoint = glb.getRpoint();
				res.obj.glb.tableX = glb.getTableX();
				res.obj.glb.tableY = glb.getTableY();
				res.obj.loc = {};
				res.obj.loc.points = loc.points;
				res.back = loc.backSaver.getState();
				res.name = "auto";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "auto")
					throw "Wrong type state";
				glb.setRpoint(res.obj.glb.Rpoint);
				glb.setTableX(res.obj.glb.tableX);
				glb.setTableY(res.obj.glb.tableY);
				loc.points = [res.obj.loc.points[0]];
				var i;
				for(i = 1; i < res.obj.loc.points.length; i++)
					loc.addPoint(res.obj.loc.points[i]);
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
		// END
			glb.start();
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- GENERATOR
		obj.objs.generator = function(){
			var glb = obj.objs.window();
			var loc = {};
			glb.setCaption("GENER");
			glb.setUPcolor("33FF33");
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			// inners
			loc.inners = [obj.objs.innerF(), obj.objs.innerF()];
				glb.getInners = function(){return loc.inners;}
			// system
			loc.gzBase = 220;
				glb.setGz = function(v){loc.gzBase = v; loc.rangeGz[1]();}
				glb.getGz = function(){return loc.gzBase;}
			loc.gz = loc.gzBase;
			loc.ctx = obj.ctx.createScriptProcessor(4096, 1, 1);
			loc.pos = 0;
			glb.start = function(){
				loc.pos = 0;
			}
			loc.ctx.onaudioprocess = function(e){
				var chn;
				for(chn = 0; chn < e.outputBuffer.numberOfChannels; chn++){
					var inp  = e.inputBuffer .getChannelData(chn);
					var outp = e.outputBuffer.getChannelData(chn);
					var i, t, d, gz, pos = loc.pos;
					for(i = 0; i < inp.length; i++){
						t = obj.ctx.currentTime + i / obj.ctx.sampleRate; // Текущее время
						gz = loc.gz * Math.pow(2, loc.inners[1].get(
							antiTrunc(t * obj.bpm / 60),
							t
						));
						d = gz / obj.ctx.sampleRate;
						pos = antiTrunc(pos + d);
						try{
							if(t == undefined)
								debugger;
							outp[i] = loc.inners[0].get(pos, t);
						}catch(err){
							debugger;
						}
					}
				}
				loc.pos = pos;
			}
			// outers
			loc.outers = [obj.objs.outerS(loc.ctx)];
				glb.getOuters = function(){return loc.outers;}
			// GUI in
			loc.rangeGz = newRange(loc.div, -12, 12, 0,
				function(v){
					loc.gz = loc.gzBase * Math.pow(2, v / 12);
					obj.log("note", v);
				},
				function(v){
					return loc.gz + " GZ";
				}
			);
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.gz = glb.getGz();
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "generator";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "generator")
					throw "Wrong type state";
				glb.setGz(res.obj.glb.gz);
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			glb.start();
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- JOIN_M
		obj.objs.joinM = function(f, s){
			var glb = obj.objs.window();
			var loc = {};
			// inners
			loc.inners = [];
			glb.getInners = function(){return loc.inners;}
			// outers
			loc.outers = [s()];
			glb.getOuters = function(){return loc.outers;}
			// system
			glb.setInners = function(count){
				count = +count;
				if(count != count || count < 0)
					throw "Wrong count of inners";
				while(loc.inners.length < count)
					loc.inners.push(f());
				var buf;
				while(loc.inners.length > count){
					buf = loc.inners.pop();
					if(buf.isConnect)
						throw "You must disconnect inner before delete it";
				}
			}
			// GUI out
			loc.div = glb.getContainer();
			loc.butSet = newEl(loc.div, "button");
				loc.butSet.innerHTML = "Set count of inners";
			loc.afterDiv = newEl(loc.div, "div");
				glb.getContainer = function(){return loc.afterDiv;}
			// GUI in
			loc.butSet.onclick = function(e){
				var res = menuInput("number",
					"Введите количество входов",
					glb.getInners().length);
				if(res == null || res < 0)
					return;
				glb.setInners(res);
			}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.Cinners = glb.getInners().length;
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "joinM";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "joinM")
					throw "Wrong type state";
				glb.setInners(res.obj.glb.Cinners);
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- JOIN_S
		obj.objs.joinS = function(){ // Пока это простое соединение в одном node
			var loc = {};
			loc.ctx = obj.ctx.createGain();
			var glb = obj.objs.joinM(
				function(){
					return obj.objs.innerS(loc.ctx);
				},
				function(){
					return obj.objs.outerS(loc.ctx);
				}
			);
			glb.setCaption("Sound join");
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "joinS";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "joinS")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- JOIN_E
		obj.objs.joinE = function(){ // Чисто насквозь
			var loc = {};
			loc.f = [function(e){
				console.log(e);
			}];
			var glb = obj.objs.joinM(
				function(){
					return obj.objs.innerE(loc.f[0]);
				},
				function(){
					return obj.objs.outerE();
				}
			);
			loc.f[0] = function(e){
				glb.getOuters()[0].run(e);
			}
			glb.setCaption("Event join");
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "joinE";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "joinE")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- SAPER
		obj.objs.saper = {};
		obj.objs.saper.one = function(){
			var glb = obj.objs.graph();
			var loc = {};
			glb.setCaption("SAPER");
			// system
			loc.runLose = function(){
				alert("You lose!");
				glb.newGame();
			}
			loc.runClick = function(x, y, but){
				loc.setState(x, y, but);
			}
			loc.runWin = function(){
				alert("YOU WIN!");
				glb.newGame();
			}
			loc.runNewGame = function(){};
			glb.setEvent = function(d, f){
				switch(d){
				case "lose":
					loc.runLose = f;
					break;
				case "click":
					loc.runClick = f;
					break;
				case "win":
					loc.runWin = f;
					break;
				case "new":
					loc.runNewGame = f;
					break;
				}
			}
			loc.field = [];
			loc.pars = [30, 16, 99]; // default
			loc.resetField = function(){
				function rand(x){
					return Math.floor(Math.random() * x);
				}
				var i;
				loc.field = [];
				for(i = 0; i < loc.pars[0] * loc.pars[1]; i++)
					loc.field[i] = 0;
				var x, y;
				for(i = 0; i < loc.pars[2]; i++){
					x = rand(loc.pars[0]);
					y = rand(loc.pars[1]);
					if(loc.field[x + y*loc.pars[0]] != 0)
						i--; // random time
					else
						loc.field[x + y*loc.pars[0]] = 1;
				}
				loc.countNoOpen = loc.pars[0] * loc.pars[1];
				loc.runNewGame();
			}
			glb.newGame = function(){
				loc.resetField();
			}
				glb.setXFsize = function(v){loc.pars[0] = v; loc.resetField();}
				glb.setYFsize = function(v){loc.pars[1] = v; loc.resetField();}
				glb.setMineCount = function(v){loc.pars[2] = v; loc.resetField();}
				glb.getXFsize = function(){return loc.pars[0];}
				glb.getYFsize = function(){return loc.pars[1];}
				glb.getMineCount = function(){return loc.pars[2];}
			loc.getState = function(x, y){
				x = +x;
				y = +y;
				function isMine(x, y){
					if(x < 0 || x >= loc.pars[0] || y < 0 || y >= loc.pars[1])
						return 0;
					if(loc.field[x + y*loc.pars[0]] == 1 || loc.field[x + y*loc.pars[0]] == 4)
						return 1;
					return 0;
				}
				var k = 0;
				switch(loc.field[x + y*loc.pars[0]]){
				case 0:
				case 1:
					return -1;
				case 2:
					k += isMine(x - 1, y - 1);
					k += isMine(x - 1, y + 0);
					k += isMine(x - 1, y + 1);
					k += isMine(x + 0, y - 1);
					k += isMine(x + 0, y + 1);
					k += isMine(x + 1, y - 1);
					k += isMine(x + 1, y + 0);
					k += isMine(x + 1, y + 1);
					return k;
				case 3:
				case 4:
					return -2;
				}
			}
			loc.setState = function(x, y, fl){
				x = +x;
				y = +y;
				switch(loc.field[x + y*loc.pars[0]]){
				case 0:
					if(fl){
						loc.field[x + y*loc.pars[0]] = 2;
						loc.countNoOpen--;
					} else
						loc.field[x + y*loc.pars[0]] = 3;
					break;
				case 1:
					if(fl){
						loc.runLose();
						return true;
					} else
						loc.field[x + y*loc.pars[0]] = 4;
					break;
				case 2:
					break;
				case 3:
					if(!fl)
						loc.field[x + y*loc.pars[0]] = 0;
					break;
				case 4:
					if(!fl)
						loc.field[x + y*loc.pars[0]] = 1;
					break;
				}
				if(loc.countNoOpen == loc.pars[2]){
					loc.runWin();
					return true;
				}
			}
			// GUI out
			loc.gr = glb.getGr();
				glb.getGr = undefined;
			glb.setStyle(0, "rgb(255, 255, 255)");
			glb.setStyle(1, "rgb(0, 255, 0)");
			glb.setStyle(2, "rgb(255, 0, 0)");
			glb.setXsize(15 * loc.pars[0]);
			glb.setYsize(15 * loc.pars[1]);
			glb.drawTrace = function(arr, arr2){
				var i;
				loc.gr.beginPath();
				var dx = glb.getXsize() / loc.pars[0];
				var dy = glb.getYsize() / loc.pars[1];
				for(i = 0; i < arr.length; i++)
					if(i == 0)
						loc.gr.moveTo(arr[i][0] * dx + dx/2, arr[i][1] * dy + dy/2);
					else
						loc.gr.lineTo(arr[i][0] * dx + dx/2, arr[i][1] * dy + dy/2);
				loc.gr.stroke();
				loc.gr.fillStyle = "rgb(0,0,0)";
				for(i = 0; i < arr2.length; i++)
					loc.gr.fillText("...", arr2[i][0] * dx, arr2[i][1] * dy + dy);
			}
			loc.draw = function(){
				loc.gr.canvas.width += 0;
				var dx = glb.getXsize() / loc.pars[0];
				var dy = glb.getYsize() / loc.pars[1];
				loc.gr.font = dy + "px Arial";
				function sq(x, y, fl){
					var txt = "";
					var col = [];
					switch(fl){
					case -2:
						txt = "F";
						col = [glb.getStyle(1), glb.getStyle(2)];
						break;
					case -1:
						txt = "?";
						col = [glb.getStyle(1), glb.getStyle(2)];
						break;
					case 0:
						txt = "";
						col = [glb.getStyle(0), glb.getStyle(1)];
						break;
					default:
						col = [glb.getStyle(0), glb.getStyle(1)];
						txt = "" + fl;
					}
					loc.gr.fillStyle = col[0];
					//loc.gr.beginPath();
					//loc.gr.rect(x*dx, y*dy, (x+1)*dx, (y+1)*dy);
					//loc.gr.stroke();
					loc.gr.fillRect(x*dx, y*dy, (x+1)*dx, (y+1)*dy);
					loc.gr.fillStyle = col[1];
					loc.gr.fillText(txt, x*dx, dy+y*dy);
				}
				var i, j;
				for(i = 0; i < loc.pars[0]; i++)
					for(j = 0; j < loc.pars[1]; j++)
						sq(i, j, loc.getState(i, j));
			}
				glb.setDraw(loc.draw);
				glb.setDraw = undefined;
				glb.draw = function(){
					loc.draw();
				}
			// GUI in
			loc.gr.canvas.oncontextmenu = function(){return false;}
			loc.gr.canvas.onmouseup = function(e){
				var getMpos = function(e){
					var Acoor = [e.clientX, e.clientY];
					var Bcoor = findCoor(e.target);
					return [
						Acoor[0] - Bcoor[0],
						Acoor[1] - Bcoor[1]
					];
				}
				var dx = glb.getXsize() / loc.pars[0];
				var dy = glb.getYsize() / loc.pars[1];
				var coor = getMpos(e);
				coor = [
					Math.floor(coor[0] / dx),
					Math.floor(coor[1] / dy)
				];
				loc.runClick(coor[0], coor[1], e.button == 0);
			}
			// global
			glb.getState = loc.getState;
			glb.setState = loc.setState;
			// END
			glb.start();
			glb.newGame();
			return filterObj(glb);
		}
		obj.objs.saper.two = function(){
			var glb = obj.objs.saper.one();
			var loc = {};
			loc.timer = -1;
			loc.Mtimer = 100000;
			loc.look = [];
			loc.log = [];
			loc.rec = function(x, y, arr){
				if(x < 0 || x >= glb.getXFsize() || y < 0 || y >= glb.getYFsize())
					return;
				var st = glb.getState(x, y);
				if(st < 0)
					return;
				if(arr.findIndex(function(v){return v[0] == x && v[1] == y;}) >= 0)
					return;
				if(loc.look.findIndex(function(v){return v[0] == x && v[1] == y;}) >= 0)
					return;
				if(loc.timer == -1){
					loc.timer = 0;
					loc.log = [];
				} else
					loc.timer++;
				if(loc.timer > loc.Mtimer)
					throw "HUI";
				loc.log.push([x, y]);
				//debugger;
				arr.push([x, y]);
					loc.rec(x - 1, y - 1, arr);
					loc.rec(x - 1, y + 0, arr);
					loc.rec(x - 1, y + 1, arr);
					loc.rec(x + 0, y - 1, arr);
					loc.rec(x + 0, y + 1, arr);
					loc.rec(x + 1, y - 1, arr);
					loc.rec(x + 1, y + 0, arr);
					loc.rec(x + 1, y + 1, arr);
				arr.pop();
				var countF = 0;
				var arrN = [];
				function gg(x, y){
					if(x < 0 || x >= glb.getXFsize() || y < 0 || y >= glb.getYFsize())
						return;
					var st = glb.getState(x, y);
					if(st == -2)
						countF++;
					if(st == -1)
						arrN.push([x, y]);
				}
					gg(x - 1, y - 1);
					gg(x - 1, y + 0);
					gg(x - 1, y + 1);
					gg(x + 0, y - 1);
					gg(x + 0, y + 1);
					gg(x + 1, y - 1);
					gg(x + 1, y + 0);
					gg(x + 1, y + 1);
				if(countF + arrN.length == st){
					arrN.findIndex(function(v){
						glb.setState(v[0], v[1], false);
						return false;
					});
					loc.look.push([x, y]);
					return;
				}
				if(countF == st){
					arrN.findIndex(function(v){
						if(glb.setState(v[0], v[1], true))
							throw "UP";
						return false;
					});
					loc.rec(x, y, arr);
					return;
				}
			}
			glb.setEvent("click", function(x, y, but){
				if(!but){
					glb.setState(x, y, false);
					return;
				}
				var st = glb.getState(x, y);
				if(st == -2)
					return;
				if(st == -1){
					if(glb.setState(x, y, true))
						return;
					st = glb.getState(x, y);
				}
				loc.timer = -1;
				try{
					loc.rec(x, y, []);
				}catch(err){
					if(err == "UP")
						return;
					glb.trace();
					setTimeout(function(){glb.start();}, 500);
				}
			});
			glb.setEvent("new", function(){
				loc.look = [];
			});
			glb.trace = function(){
				glb.stop();
				glb.draw();
				glb.drawTrace(loc.log, loc.look);
			}
			return glb;
		}	
		obj.objs.saperCall = function(){
			var glb = obj.objs.saper.two();
			var loc = {};
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "saper";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "saper")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// Inners & Outers
			glb.getInners = function(){return [];}
			glb.getOuters = function(){return [];}
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- VIDEO
		obj.objs.video = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			glb.setCaption("Player: ~none~");
			// GUI out
			loc.vH = newEl(loc.div, "video");
				loc.vH.controls = false;
				loc.vH.muted = true;
				loc.vH.loop = true;
			loc.vHsize = [0, 0];
				glb.setVXsize = function(v){loc.vHsize[0] = v; loc.vH.width  = v;};
				glb.getVXsize = function(){return loc.vHsize[0];};
				glb.setVYsize = function(v){loc.vHsize[1] = v; loc.vH.height = v;};
				glb.getVYsize = function(){return loc.vHsize[1];};
				glb.setVXsize(320); // default
				glb.setVYsize(240);
			loc.butLoad = newEl(loc.div, "button");
				loc.butLoad.innerHTML = "LOAD";
			loc.SH = function(state){
				switch(state){
				case "load":
					loc.butLoad.style.display = "block";
					loc.vH     .style.display = "none";
					break;
				case "video":
					loc.butLoad.style.display = "block";
					loc.vH     .style.display = "block";
					break;
				case "loading":
					loc.butLoad.style.display = "none";
					loc.vH     .style.display = "none";
					break;
				}
			}
			loc.SH("load");
			// system
			loc.arrayB = null;
			loc.loadArrayB = function(arr, file){ // file.name & file.type
				loc.goLoad(
					window.URL.createObjectURL(
						new Blob([arr], file)),
					function(){
						obj.ctx.decodeAudioData(arr)
							.then(function(e){
								loc.SH("video");
								loc.sound.setSnd(e);
								glb.setCaption("File: <i>" +
									cutStr(file.name, 15) +
									"</i>");
								loc.arrayB = [arr, file];
							}).catch(function(e){
								loc.SH("load");
								glb.setCaption("Player: ~audio error~");
							});
					},
					function(){
						glb.setCaption("Player: ~video error~");
						loc.SH("load");
					}
				);
			}
			loc.fileLoad = newFileLoad(loc.div, function(arr){
				if(arr.length <= 0)
					return;
				glb.setCaption("Player: ~loading~");
				loc.SH("loading");
				var f = new FileReader();
				f.onload = function(e){
					loc.loadArrayB(f.result, arr[0]);
				}
				f.readAsArrayBuffer(arr[0]);
			});
			loc.goLoad = function(file, cbsucs, cberr){
				loc.vH.addEventListener("loadeddata", cbsucs);
				loc.vH.addEventListener("error", cberr);
				loc.vH.src = file;
			}
			glb.start = function(t){
				if(t == undefined)
					t = 0;
				t = +t;
				if(t != t)
					return;
				if(t < 0)
					t = 0;
				if(t > loc.vH.duration)
					t = loc.vH.duration;
				loc.sound.start(t);
				loc.vH.currentTime = t;
				if(loc.vH.paused)
					loc.vH.play().catch(function(){
						glb.setCaption("Player: ~warn~");
					});
			}
			glb.stop = function(){
				loc.sound.stop();
				loc.vH.pause();
			}
			glb.getDuration = function(){
				return loc.vH.duration;
			}
			// GUI in
			loc.butLoad.onclick = function(e){
				loc.fileLoad.click();
			}
			loc.vH.onclick = function(e){
				if(loc.vH.paused)
					glb.start(loc.vH.currentTime);
				else
					glb.stop();
			}
			// modules
			loc.sound = obj.objs.file();
				loc.sound.setWState(null); // hide
				glb.getSound = loc.sound.getSound;
				glb.getOuters = loc.sound.getOuters;
			// inners
			loc.inners = [obj.objs.innerE(function(e){
				if(e.t != undefined)
					glb.start(e.t);
				else
					glb.stop();
			})];
				glb.getInners = function(){return loc.inners;}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.VXsize = glb.getVXsize();
				res.obj.glb.VYsize = glb.getVYsize();
				res.obj.loc = {};
				res.obj.loc.arrayB = loc.arrayB;
				if(loc.arrayB != null){
					res.obj.loc.arrayB = [
						JSONencode(loc.arrayB[0]),
						{
							name: loc.arrayB[1].name,
							type: loc.arrayB[1].type
						}
					];
				}
				res.back = loc.backSaver.getState();
				res.name = "video";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "video")
					throw "Wrong type state";
				glb.setVXsize(res.obj.glb.VXsize);
				glb.setVYsize(res.obj.glb.VYsize);
				loc.arrayB = res.obj.loc.arrayB;
				if(loc.arrayB != null){
					loc.arrayB = [
						JSONdecode(res.obj.loc.arrayB[0]),
						res.obj.loc.arrayB[1]
					];
					loc.loadArrayB(loc.arrayB[0], loc.arrayB[1]);
				}
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- CUTTER
		obj.objs.cutter = function(){
			var glb = obj.objs.window();
			var loc = {};
			glb.setCaption("Cutter");
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
		// local
			loc.strSetName = "Enter the name:";
			loc.strDefName = "Pat";
			loc.strDescExport = '' +
				'Здесь вы можете экспортировать<br>' +
				'вашу нарезку в Vegas Pro <br>' +
				'(тестировано на версии 10.0)<br>' +
				'Для начала ' +
				'<a href="f/cutterSV.js" download title="Да нажми уже)">скачайте</a> ' +
				'скрипт для Sony Vegas<br>' +
				'Затем в Vegas ' +
				'выделите нужный клип для нарезки,<br>' +
				'<a href="f/cutterSV.png" target="__blank" title="Скриншот в новом окне">откройте</a> ' +
				'окно выбора скриптов<br>' +
				'и откройте скачанный скрипт<br>' +
				'Затем скопируйте текст ниже<br>' +
				'и вставьте его в открывшееся в Vegas окно<br>' +
				'после чего нажмите <b>OK</b><br>' +
				'Сообщение <i>I can\'t continue</i> означает,<br>' +
				'что скрипт не смог получить вставленный текст<br>' +
				'Сообщение <i>Wrong data</i> означает некорректный<br>' +
				'вставленный текст<br>' +
				'Сообщение <i>Sucs!</i> означает успешное<br>' +
				'выполнение скрипта, и после него вы должны<br>' +
				'на timeline увидеть после выделенного клипа<br>' +
				'нарезанный клип.<br>' +
				'<b>Проблема не решается - свяжитесь со мной!</b>';
			loc.helpRec = [ // Помощь по режиму записи
				["Left", "Move cursor to left"],
				["Right", "Move cursor to right"],
				["Space", "Play fragment at cursor"],
				["Enter", "Save fragment"],
				["Q", "Exit from record without save"]
			];
			loc.helpPat = [
				["Left", "Add pat from nowlist to alllist after selected"],
				["Right", "Add pat from alllist to nowlist after selected"],
				["Delete", "Remove selected pat from selected list"],
				["Space", "Add pause to nowlist"]
			];
		// system
			loc.deltaRec = 0.5; // Сколько воспроизводится обособленный кусок в секундах
				glb.getPatDefTime = function(){return loc.deltaRec;}
				glb.setPatDefTime = function(v){loc.deltaRec = v;}
			loc.times = []; // Паттерны
			loc.helpCol = "rgb(50,50,255)"; // Цвет нажатой клавиши
				glb.getColKey = function(){return loc.helpCol;}
				glb.setColKey = function(v){loc.helpCol = v;}
			loc.patt = []; // Плейлист
			loc.sizeList = 10; // Строчек в списках
				glb.getListSize = function(){return loc.sizeList;}
				glb.setListSize = function(v){loc.sizeList = v;}
			loc.playT = 1; // Фрагментов в бите
				glb.getPatPerBit = function(){return loc.playT;}
				glb.setPatPerBit = function(v){loc.playT = v;}
		// outers
			loc.outers = [obj.objs.outerE()];
				glb.getOuters = function(){return loc.outers;}
		// inners
			glb.getInners = function(){return [];}
		// GUI out
			loc.butRec = norm(newEl(loc.div, "button")); // Кнопка записи фрагментов
				loc.butRec.innerHTML = "RECORD";
			loc.butStopRec = newEl(loc.div, "button"); // Кнопка остановки записи и сохранения фрагментов
				loc.butStopRec.innerHTML = "SAVE";
			loc.onRangeD = function(v){};
			loc.rangeD = newRange(loc.div, -50, 50, 0, // Сдвиг
				function(v){
					loc.onRangeD(v / 50 * loc.deltaRec);
				},
				function(v){
					return "Delta: " + v / 50 * loc.deltaRec;
				}
			);
				loc.rangeDobj = loc.rangeD[0];
			
			loc.butPat = norm(newEl(loc.div, "button")); // Кнопка набора паттернов
				loc.butPat.innerHTML = "PATTERN";
			loc.butStopPat = newEl(loc.div, "button"); // Кновка возврата на главную
				loc.butStopPat.innerHTML = "BACK";
			loc.butPatPlay = newEl(loc.div, "button"); // Проиграть паттерны
				loc.butPatPlay.innerHTML = "PLAY";
			loc.butPatStop = newEl(loc.div, "button"); // Остановить проигр фрагментов
				loc.butPatStop.innerHTML = "STOP";
			loc.listPat = newEl(loc.div, "div"); // Два списка
			loc.listAllPat = loc.listNowPat = null; // Они сами
			(function(){ // Их создание
				var table = norm(newEl(loc.listPat, "table"));
				var tr = norm(newEl(table, "tr"));
				var td = [
					norm(newEl(tr, "td")),
					norm(newEl(tr, "td"))
				];
				loc.listAllPat = norm(newEl(td[0], "select"));
					loc.listAllPat.size = loc.sizeList;
				loc.listNowPat = norm(newEl(td[1], "select"));
					loc.listNowPat.size = loc.sizeList;
			})();
			loc.outListPat = function(){
				function newHTML(arr){
					var i;
					var str = "";
					for(i = 0; i < arr.length; i++)
						str += '<option>' + arr[i][0] + '</option>';
					return str;
				}
				loc.listAllPat.innerHTML = newHTML(loc.times);
				loc.listNowPat.innerHTML = newHTML(loc.patt);
			}
			
			loc.butExport = newEl(loc.div, "button"); // Кнопка экспорта
				loc.butExport.innerHTML = "EXPORT";
			loc.butBExport = newEl(loc.div, "button"); // Кнопка возврта из экспорта
				loc.butBExport.innerHTML = "BACK";
			loc.descExport = newEl(loc.div, "div"); // Описание
				loc.descExport.innerHTML = loc.strDescExport;
			loc.txtExport = newEl(loc.div, "textarea"); // Текст для экспорта
				loc.txtExport.cols = 50;
				loc.txtExport.rows = 5;
			
			loc.descTable = newEl(loc.div, "div"); // Таблица клавиш
			loc.outDesc = function(arr, ind){ // Вывод таблицы помощь
				var str = '<table>' +
					'<tr><th>Key</th><th style="width:200px">Description</th></tr>';
				var i;
				for(i = 0; i < arr.length; i++)
					str += '<tr><td' +
						((i == ind) ? (' style="background-color:' + loc.helpCol + '"') : '') +
						'>' +
						arr[i][0] + '</td><td>' +
						arr[i][1] + '</td></tr>';
				str += '</table>';
				loc.descTable.innerHTML = str;
			}
			loc.SH = function(state){
				var arr = [
					["butRec",	1,0,0,0],
					["butStopRec",	0,1,0,0],
					["rangeDobj",	0,1,0,0],
					["butPat",	1,0,0,0],
					["butStopPat",	0,0,1,0],
					["butPatPlay",	0,0,1,0],
					["butPatStop",	0,0,0,0],
					["listPat",	0,0,1,0],
					["descTable",	0,1,1,0],
					["butExport",	1,0,0,0],
					["butBExport",	0,0,0,1],
					["descExport",	0,0,0,1],
					["txtExport",	0,0,0,1]
				];
				var res = {
					start: 1,
					rec: 2,
					pat: 3,
					export: 4
				};
				if(res[state] == undefined)
					return;
				var i;
				for(i = 0; i < arr.length; i++)
					loc[arr[i][0]].style.display = (arr[i][res[state]] == 1) ? "block" : "none";
			}
				loc.SH("start");
		// GUI in
			loc.butRec.onclick = function(e){
				loc.SH("rec");
				loc.outDesc(loc.helpRec);
				var pos = 0;
				var Dpos = 0;
				var times = [];
				w.onkeydown = function(e){
					switch(e.keyCode){
					case 37:
						loc.outDesc(loc.helpRec, 0);
						break;
					case 39:
						loc.outDesc(loc.helpRec, 1);
						break;
					case 32:
						loc.outDesc(loc.helpRec, 2);
						break;
					case 13:
						loc.outDesc(loc.helpRec, 3);
						break;
					case 27:
						loc.outDesc(loc.helpRec, 4);
						break;
					}
				}
				w.onkeyup = function(e){
					var playTimer = null;
					function play(){
						if(playTimer != null)
							w.clearTimeout(playTimer);
						loc.outers[0].run({t: pos + Dpos});
						playTimer = w.setTimeout(function(){
							loc.outers[0].run();
							playTimer = null;
						}, loc.deltaRec*1000);
					}
					loc.outDesc(loc.helpRec);
					switch(e.keyCode){
					case 37: // Left
						pos -= loc.deltaRec;
						if(pos < 0)
							pos = 0;
						play();
						break;
					case 39: // Right
						pos += loc.deltaRec;
						play();
						break;
					case 32: // Space
						play();
						break;
					case 13: // Enter
						var str = prompt(
							loc.strSetName + "<" + (pos + Dpos) + ">",
							loc.strDefName + " #" + (times.length + loc.times.length + 1));
						if(str == null)
							break;
						times.push([str, pos + Dpos]);
						break;
					case 81: // Q
						loc.SH("start");
						w.onkeyup = null;
						w.onkeydown = null;
						break;
					}
				}
				loc.getTimes = function(){return times;};
				loc.onRangeD = function(v){
					Dpos = v;
				}
				loc.rangeD[1](0);
			}
			loc.butStopRec.onclick = function(e){
				loc.SH("start");
				w.onkeyup = null;
				w.onkeydown = null;
				loc.times = loc.times.concat(loc.getTimes());
			}
			loc.butPat.onclick = function(e){
				loc.SH("pat");
				loc.outListPat();
				loc.outDesc(loc.helpPat);
				var playTimer = null;
				function play(pos){
					if(playTimer != null)
						w.clearTimeout(playTimer);
					loc.outers[0].run({t: pos});
					playTimer = w.setTimeout(function(){
						loc.outers[0].run();
						playTimer = null;
					}, loc.deltaRec*1000);
				}
				w.onkeydown = function(e){
					switch(e.keyCode){
					case 37: // Left
						loc.outDesc(loc.helpPat, 0);
						break;
					case 39: // Right
						loc.outDesc(loc.helpPat, 1);
						break;
					case 46: // Delete
						loc.outDesc(loc.helpPat, 2);
						break;
					case 32: // Space
						loc.outDesc(loc.helpPat, 3);
						break;
					}
				}
				var curr = [0,0];
				w.onkeyup = function(e){
					loc.outDesc(loc.helpPat);
					var l, r;
					switch(e.keyCode){
					case 37: // Left
						l = loc.listAllPat.selectedIndex;
						r = loc.listNowPat.selectedIndex;
						if(l < 0)
							l = loc.times.length-1;
						if(r < 0)
							break;
						if(loc.patt[r][1] == undefined){
							alert("What? Why do you do this?");
							break;
						}
						loc.times = loc.times.slice(0, l+1).
							concat([loc.patt[r]]).
							concat(loc.times.slice(l+1));
						loc.outListPat();
						break;
					case 39: // Right
						l = loc.listAllPat.selectedIndex;
						r = loc.listNowPat.selectedIndex;
						if(l < 0)
							break;
						if(r < 0)
							r = loc.patt.length-1;
						loc.patt = loc.patt.slice(0, r+1).
							concat([loc.times[l]]).
							concat(loc.patt.slice(r+1));
						loc.outListPat();
						break;
					case 46: // Delete
						if(curr[0] && curr[1])
							throw "WAT";
						if(!curr[0] && !curr[1])
							break;
						if(curr[0]){
							l = loc.listAllPat.selectedIndex;
							if(l < 0)
								l = loc.times.length-1;
							loc.times = loc.times.slice(0, l).concat(
								loc.times.slice(l+1));
						} else {
							r = loc.listNowPat.selectedIndex;
							if(r < 0)
								r = loc.patt.length-1;
							loc.patt = loc.patt.slice(0, r).concat(
								loc.patt.slice(r+1));
						}
						loc.outListPat();
						break;
					case 32: // Space
						r = loc.listNowPat.selectedIndex;
						if(r < 0)
							r = loc.patt.length - 1;
						loc.patt = loc.patt.slice(0, r+1).
							concat([["Delay"]]).
							concat(loc.patt.slice(r+1));
						loc.outListPat();
						break;
					}
				}
				loc.listAllPat.onfocus = function(e){curr[0] = 1;}
				loc.listNowPat.onfocus = function(e){curr[1] = 1;}
				loc.listAllPat.onblur = function(e){curr[0] = 0;}
				loc.listNowPat.onblur = function(e){curr[1] = 0;}
				loc.listAllPat.onclick = function(e){
					var ind = loc.listAllPat.selectedIndex;
					if(ind < 0)
						return;
					play(loc.times[ind][1]);
				}
				loc.listNowPat.onclick = function(e){
					var ind = loc.listNowPat.selectedIndex;
					if(ind < 0)
						return;
					play(loc.patt[ind][1]);
				}
				var timer = null;
				var startT = 0;
				loc.butPatPlay.onclick = function(e){
					if(loc.patt.length == 0){
						alert("You are empty!");
						return;
					}
					loc.butPatPlay.style.display = "none";
					loc.butPatStop.style.display = "block";
					startT = obj.ctx.currentTime;
					var lastPos = loc.patt.length - 1;
					timer = w.setInterval(function(){
						var t = obj.ctx.currentTime - startT; // Время в секундах с начала запуска
						var bps = obj.bpm / 60 * loc.playT; // Паттернов в секунду
						var pos = Math.floor(t * bps) % loc.patt.length; // Позиция
						if(pos == lastPos)
							return;
						lastPos = pos;
						loc.listNowPat.selectedIndex = pos;
						if(loc.patt[pos][1] == undefined)
							return;
						loc.outers[0].run({t: loc.patt[pos][1]});
					}, 10);
				}
				loc.butPatStop.onclick = function(e){
					loc.butPatPlay.style.display = "block";
					loc.butPatStop.style.display = "none";
					w.clearInterval(timer);
					timer = null;
					loc.outers[0].run();
				}
			}
			loc.butStopPat.onclick = function(e){
				loc.butPatStop.click();
				loc.SH("start");
				w.onkeyup = null;
				w.onkeydown = null;
			}
			loc.butExport.onclick = function(e){
				if(loc.patt.length == 0){
					alert("You are empty!");
					return;
				}
				loc.SH("export");
				function getTxt(){
					var dt = 1 / (obj.bpm / 60 * loc.playT);
					var arr = [];
					for(i = 0; i < loc.patt.length; i++){
						arr[i] = loc.patt[i][1];
						if(arr[i] == undefined)
							arr[i] = "u";
					}
					arr.unshift(dt);
					return arr.join("!");
				}
				loc.txtExport.innerHTML = getTxt();
				loc.txtExport.select();
			}
			loc.butBExport.onclick = function(e){
				loc.SH("start");
			}
		// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.patDefTime = glb.getPatDefTime();
				res.obj.glb.colKey = glb.getColKey();
				res.obj.glb.listSize = glb.getListSize();
				res.obj.glb.patPerBit = glb.getPatPerBit();
				
				res.obj.loc = {};
				res.obj.loc.times = loc.times;
				res.obj.loc.patt = loc.patt;
				
				res.back = loc.backSaver.getState();
				res.name = "cutter";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "cutter")
					throw "Wrong type state";
				glb.setPatDefTime(res.obj.glb.patDefTime);
				glb.setColKey(res.obj.glb.colKey);
				glb.setListSize(res.obj.glb.listSize);
				glb.setPatPerBit(res.obj.glb.patPerBit);
				
				loc.times = res.obj.loc.times;
				loc.patt = res.obj.loc.patt; // null => undefined
				
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
		// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- INPUT
		obj.objs.input = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			glb.setCaption("INNER");
			// system
			loc.ctx = obj.ctx.createGain();
			loc.check = function(){
				if(w.navigator == undefined)
					return false;
				if(w.navigator.mediaDevices == undefined)
					return false;
				return true;
			}
			loc.bufStop = [];
			loc.start = function(){
				loc.SH("loading");
				glb.setCaption("Connect...");
				w.navigator.mediaDevices.getUserMedia({audio: true}).
					then(function(stream){
						var arr = stream.getAudioTracks();
						if(arr.length == 0){
							glb.setCaption("No audio tracks!");
							loc.SH("connect");
							return;
						}
						loc.bufStop = [
							arr[0],
							obj.ctx.createMediaStreamSource(stream)
						];
						loc.bufStop[1].connect(loc.ctx);
						glb.setCaption("INNER");
						loc.SH("disconnect");
					}).catch(function(e){
						glb.setCaption("I don't get your device!");
						loc.SH("connect");
					});
			}
			loc.stop = function(){
				loc.bufStop[1].disconnect(loc.ctx);
				loc.bufStop[0].stop();
				loc.butStop = [];
				loc.SH("connect");
			}
			// GUI out
			loc.buts = {
				connect:	newEl(loc.div, "button"),
				disconnect:	newEl(loc.div, "button")
			};
				loc.buts.connect.	innerHTML = "CONNECT";
				loc.buts.disconnect.	innerHTML = "DISCONNECT";
			loc.SH = function(state){
				switch(state){
				case "connect":
					loc.buts.connect	.style.display = "block";
					loc.buts.disconnect	.style.display = "none";
					break;
				case "disconnect":
					loc.buts.connect	.style.display = "none";
					loc.buts.disconnect	.style.display = "block";
					break;
				case "loading":
					loc.buts.connect	.style.display = "none";
					loc.buts.disconnect	.style.display = "none";
					break;
				}
			}
				loc.SH("connect");
			// GUI in
			loc.buts.connect.onclick = function(e){
				loc.start();
			}
			loc.buts.disconnect.onclick = function(e){
				loc.stop();
			}
			// Outers
			loc.outers = [obj.objs.outerS(loc.ctx)];
				glb.getOuters = function(){return loc.outers;}
			// Inners
				glb.getInners = function(){return [];}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "input";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "input")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- PATCHER
		obj.objs.patcher = function(){
			var glb = obj.objs.graph();
			var loc = {};
			loc.gr = glb.getGr();
				glb.getGr = undefined;
			glb.setCaption("Patcher");
		// props
			loc.sizeSQ = 30;
				glb.setSizeSQ = function(v){loc.sizeSQ = v;}
				glb.getSizeSQ = function(){return loc.sizeSQ;}
		// menu
			loc.menu = {};
			loc.menu.main = function(){
				return newMenu([
					["Add...", function(){
						loc.menu.add().open();
					}],
					["Set...", function(){
						loc.menu.set().open();
					}],
					["File...", function(){
						loc.menu.file().open();
					}]
				]);
			}
			loc.menu.obj = function(ind){
				var arr = [];
				if(loc.arr[ind].state.isHide)
					arr.push(["Show", function(){
						loc.arr[ind].obj.setWState(true);
						loc.arr[ind].state.isHide = false;
					}]);
				else
					arr.push(["Hide", function(){
						loc.arr[ind].obj.setWState(null);
						loc.arr[ind].state.isHide = true;
					}]);
				if(loc.connected == null){
					if(ind > 0)
						arr.push(["Start connect...", function(){
							loc.menu.objStartConnect(ind).open();
						}]);
				} else
					arr.push(["Stop connect...", function(){
						loc.menu.objStopConnect(ind).open();
					}]);
				if(ind > 0)
					arr.push(["Delete", function(){
						var res = w.confirm("Are you sure?");
						if(!res)
							return;
						loc.remDevice(ind);
					}]);
				return newMenu(arr);
			}
			loc.menu.add = function(){
				var arr = [];
				var i;
				function getF(a){
					return function(){
						loc.addDevice(a);
					}
				}
				for(i in obj.validObjs)
					arr.push([i, getF(obj.validObjs[i])]);
				return newMenu(arr);
			}
			loc.menu.set = function(){
				return newMenu([
					["HEHE", function(){}]
				]);
			}
			loc.menu.objStartConnect = function(ind){
				if(loc.arr[ind].obj.getOuters == undefined)
					throw loc.arr[ind].obj;
				var outers = loc.arr[ind].obj.getOuters();
				if(outers.length == 0){
					return newMenu([
						["No outers!", function(){}]
					]);
				}
				function getN(ind){
					return (1 + +ind) + " : " + outers[ind].type;
				}
				function getF(indd){
					return function(){
						loc.connected = [outers[indd], ind, indd];
					}
				}
				var i;
				var arr = [];
				for(i = 0; i < outers.length; i++)
					arr.push([getN(i), getF(i)]);
				if(arr.length == 1){
					arr[0][1]();
					return newMenu();
				}
				return newMenu(arr);
			}
			loc.menu.objStopConnect = function(ind){
				if(loc.arr[ind].obj.getInners == undefined)
					throw loc.arr[ind].obj;
				var Binners = loc.arr[ind].obj.getInners();
				var i;
				var inners = [];
				for(i = 0; i < Binners.length; i++)
					if(Binners[i].type == loc.connected[0].type)
						inners.push([i, Binners[i]]);
				if(inners.length == 0){
					loc.connected = null;
					return newMenu([
						["No inners!", function(){}]
					]);
				}
				function getN(ind){
					return (1 + +inners[ind][0]) + " : " + (
					(inners[ind][1].isConnect ? "disconnect" : "connect"));
				}
				function getF(indd){
					return function(){
						if(inners[indd][1].isConnect){
							try{
								loc.disconnect(loc.connected[1], ind);
								loc.connected[0].disconnect(inners[indd][1]);
							}catch(err){
								alert("Wrong operation: " + err);
								console.error(err);
								loc.connected = null;
								return;
							}
						} else {
							try{
								glb.connect(loc.connected[1], ind, loc.connected[2], inners[indd][0]);
								/*
								loc.connect(loc.connected[1], ind, {
									type: loc.connected[0].type,
									inner: inners[indd][0],
									outer: loc.connected[2]
								});
								loc.connected[0].connect(inners[indd][1]);
								*/
							}catch(err){
								alert("Wrong operation: " + err);
								console.error(err);
								loc.connected = null;
								return;
							}
						}
						loc.connected = null;
					}
				}
				var arr = [];
				for(i = 0; i < inners.length; i++)
					arr.push([getN(i), getF(i)]);
				if(arr.length == 1){
					arr[0][1]();
					return newMenu();
				}
				return newMenu(arr);
			}
			loc.menu.fileObj = (function(){
				var glb = obj.objs.window();
				var loc = {};
				glb.setCaption("Copypaste");
				glb.setUPcolor("00FF00");
				glb.setPosX(300);
				glb.setPosY(0);
				glb.setWState(null);
				loc.div = glb.getContainer();
				loc.but = newEl(loc.div, "button");
					loc.but.innerHTML = "OK";
				loc.txt = newEl(loc.div, "textarea");
					loc.txt.cols = "50";
					loc.txt.rows = "20";
					loc.txt.onclick = function(e){
						loc.txt.select();
					}
				glb.go = function(txt, onOK){
					if(onOK == undefined)
						onOK = function(){};
					loc.txt.value = txt;
					loc.but.onclick = function(e){
						onOK(loc.txt.value);
						glb.setWState(null);
					}
					glb.setWState(true);
					loc.txt.select();
				}
				return glb;
			})();
			loc.menu.file = function(){
				return newMenu([
					["Save", function(){
						loc.menu.fileObj.go(JSON.stringify(glb.getSaver().getState()));
					}],
					["Load", function(){
						loc.menu.fileObj.go("Paste code here", function(txt){
							if(txt == "" || txt == "Paste code here")
								return;
							var res = {};
							try{
								res = JSON.parse(txt);
							}catch(err){
								alert("Wrong code!");
								console.error(err);
								return;
							}
							glb.getSaver().setState(res);
						});
					}],
					["Samples...", function(){
						loc.menu.fileSamples().open();
					}]
				]);
			}
			loc.menu.fileSamples = function(){
				var arr = [];
				var i;
				function getF(ind){
					return function(){
						glb.getSaver().setState(JSON.parse(patcherSamples[ind][1]));
					}
				}
				for(i = 0; i < patcherSamples.length; i++)
					arr.push([
						patcherSamples[i][0], getF(i)
					]);
				return newMenu(arr);
			}
		// system
			glb.connect = function(startObj, stoppObj, startCh, stoppCh){
				//loc.arr[startObj].obj.getInners()[startCh]
				//loc.arr[stoppObj].obj.getOuters()[stoppCh]
				loc.connect(startObj, stoppObj, {
					type: loc.arr[startObj].obj.getOuters()[startCh].type,
					inner: stoppCh,
					outer: startCh
				});
				loc.arr[startObj].obj.getOuters()[startCh].connect(
					loc.arr[stoppObj].obj.getInners()[stoppCh]
				);
			}
			loc.arr = [];
			loc.ctx = obj.ctx.createGain();
			loc.connected = null;
			loc.connect = function(a, b, type){
				if(loc.arr[a].connected[b] != undefined)
					throw "Cannot duplic links";
				loc.arr[a].connected[b] = type;
			}
			loc.disconnect = function(a, b){
				if(loc.arr[a].connected[b] == undefined)
					throw "Cannot empty disconnect";
				loc.arr[a].connected[b] = undefined;
			}
			loc.addDevice = function(name){
				var el = {};
				el.obj = obj.objs[name]();
				el.state = {};
				el.state.isHide = false;
				el.state.coor = [loc.sizeSQ/2, loc.sizeSQ/2];
				el.connected = [];
				loc.arr.push(el);
				return loc.arr.length - 1;
			}
				glb.addDevice = loc.addDevice;
			loc.addDevice("joinS");
				glb.getOuters = loc.arr[0].obj.getOuters;
				glb.getInners = function(){return [];}
				loc.arr[0].obj.setInners(1);
				loc.arr[0].obj.setWState(null);
				loc.arr[0].state.isHide = true;
			loc.remDevice = function(ind){
				if(loc.arr[ind] == undefined)
					return;
				var el = loc.arr[ind].obj;
				loc.arr[ind] = undefined;
				var inners = el.getInners();
				var outers = el.getOuters();
				var i;
				for(i = 0; i < inners.length; i++)
					if(inners[i].outer != null)
						inners[i].outer.disconnect(inners[i]);
				for(i = 0; i < outers.length; i++)
					outers[i].disAll();
				for(i = 0; i < loc.arr.length; i++)
					if(loc.arr[i] != undefined)
						loc.arr[i].connected[ind] = undefined;
				el.setWState(null); // Пока так
			}
				glb.remDevice = loc.remDevice;
			glb.getDevice = function(ind){
				var el = loc.arr[ind];
				if(el == undefined){
					return null;
				}
				return el.obj;
			}
			glb.setWStateDevice = function(ind, state){
				loc.arr[ind].obj.setWState(state ? true : null);
				loc.arr[ind].state.isHide = !state;
			}
			glb.setPosXDevice = function(ind, v){
				loc.arr[ind].state.coor[0] = v;
			}
			glb.setPosYDevice = function(ind, v){
				loc.arr[ind].state.coor[1] = v;
			}
		// GUI out
			glb.setStyle(0, "rgb(0,0,0)");
			glb.setStyle(1, "rgb(255,0,0)");
			glb.setXsize(500);
			glb.setYsize(500);
			glb.setDraw(function(){
				loc.gr.canvas.width+=0;
				var i, j;
				for(i = 0; i < loc.arr.length; i++){
					if(loc.arr[i] == undefined)
						continue;
					loc.gr.beginPath();
					loc.gr.strokeStyle = glb.getStyle(0);
					loc.gr.rect(
						loc.arr[i].state.coor[0] - loc.sizeSQ/2,
						loc.arr[i].state.coor[1] - loc.sizeSQ/2,
						loc.sizeSQ, loc.sizeSQ
					);
					loc.gr.font = "10px Monospace";
					loc.gr.fillText(
						loc.arr[i].obj.getCaption(),
						loc.arr[i].state.coor[0] + loc.sizeSQ/2 + 5,
						loc.arr[i].state.coor[1] - loc.sizeSQ/2 + 10
					);
					loc.gr.stroke();
					for(j = 0; j < loc.arr[i].connected.length; j++){
						if(loc.arr[i].connected[j] == undefined)
							continue;
						loc.gr.strokeStyle = glb.getStyle(1); // Цвет для разного типа соединений
						loc.gr.beginPath();
						loc.gr.moveTo(
							loc.arr[i].state.coor[0] + loc.sizeSQ/2,
							loc.arr[i].state.coor[1] - loc.sizeSQ/2
						);
						loc.gr.lineTo(
							loc.arr[j].state.coor[0],
							loc.arr[j].state.coor[1]
						);
						loc.gr.stroke();
					}
				}
			});
				glb.setDraw = undefined;
		// GUI in
			loc.gr.canvas.oncontextmenu = function(e){return false;}
			loc.gr.canvas.onmousedown = function(e){
				var coorEl = findCoor(loc.gr.canvas);
				var coorMouse = [e.clientX, e.clientY];
				var coor = [
					coorMouse[0] - coorEl[0],
					coorMouse[1] - coorEl[1]
				];
				var ind = loc.arr.findIndex(function(e){
					return(
						(e != undefined) &&
						(Math.abs(e.state.coor[0] - coor[0]) <= loc.sizeSQ/2) &&
						(Math.abs(e.state.coor[1] - coor[1]) <= loc.sizeSQ/2));
				});
				if(e.button == 0){
					if(ind < 0){
						w.onmouseup = function(e){
							w.onmouseup = null;
							loc.menu.add().open(coorMouse);
						}
						return;
					}
					var coorS = coorMouse;
					var coorF = coorS;
					w.onmousemove = function(e){
						coorF = [e.clientX, e.clientY];
						loc.arr[ind].state.coor[0] += coorF[0] - coorS[0];
						loc.arr[ind].state.coor[1] += coorF[1] - coorS[1];
						coorS = coorF;
					}
					w.onmouseup = function(e){
						w.onmousemove = null;
						w.onmouseup = null;
					}
				} else {
					if(ind < 0)
						w.onmouseup = function(e){
							w.onmouseup = null;
							loc.menu.main().open(coorMouse);
						}
					else
						w.onmouseup = function(e){
							w.onmouseup = null;
							loc.menu.obj(ind).open(coorMouse);
						}
				}
			}
		// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.glb.sizeSQ = glb.getSizeSQ();
				res.obj.loc = {};
				res.obj.loc.arr = [];
				var i;
				for(i = 0; i < loc.arr.length; i++){
					if(loc.arr[i] == undefined){
						res.obj.loc.arr[i] = undefined;
						continue;
					}
					res.obj.loc.arr[i] = {
						obj: loc.arr[i].obj.getSaver().getState(),
						state: loc.arr[i].state,
						connected: loc.arr[i].connected
					};
				}
				res.back = loc.backSaver.getState();
				res.name = "patcher";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "patcher")
					throw "Wrong type state";
				glb.setSizeSQ(res.obj.glb.sizeSQ);
				var i;
				for(i = 1; i < loc.arr.length; i++)
					if(loc.arr[i] != undefined)
						loc.remDevice(i);
				loc.arr = [loc.arr[0]];
				for(i = 0; i < res.obj.loc.arr.length; i++){
					if(res.obj.loc.arr[i] == null){
						loc.arr.push(null);
						continue;
					}
					var j = i;
					if(i > 0){
						loc.addDevice(res.obj.loc.arr[i].obj.name);
						j = loc.arr.length-1;
					}
					loc.arr[j].obj.getSaver().setState(res.obj.loc.arr[i].obj);
					loc.arr[j].state = res.obj.loc.arr[i].state;
					if(loc.arr[j].state.isHide)
						loc.arr[j].obj.setWState(null);
					else
						loc.arr[j].obj.setWState(true);
					loc.arr[j].connected = res.obj.loc.arr[i].connected;
				}
				for(i = 1; i < loc.arr.length; i++){
					if(loc.arr[i] == undefined)
						continue;
					var j;
					for(j = 0; j < loc.arr[i].connected.length; j++){
						if(loc.arr[i].connected[j] == null){
							loc.arr[i].connected[j] = undefined;
							continue;
						}
						loc.arr[i].obj.getOuters()[
							loc.arr[i].connected[j].outer
						].connect(
							loc.arr[j].obj.getInners()[
								loc.arr[i].connected[j].inner
						]);
					}
				}
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			glb.start();
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- RECORDER
		obj.objs.recorder = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			glb.setCaption("Recorder");
		// props
			loc.blob = null;
			loc.drvData = [];
			loc.bitRateMP3 = 320;
				glb.getBitRateMP3 = function(){return loc.bitRateMP3;}
				glb.setBitRateMP3 = function(v){loc.bitRateMP3 = v;}
		// GUI out
			loc.buts = {
				start:	newEl(loc.div, "button"),
				stop:	newEl(loc.div, "button"),
				downl:	newEl(loc.div, "button")
			};
			loc.aud = newEl(loc.div, "audio");
				loc.aud.controls = "true";
			loc.buts.start	.innerHTML = "START";
			loc.buts.stop	.innerHTML = "STOP";
			loc.buts.downl	.innerHTML = "Download MP3";
			loc.SH = function(state){
				switch(state){
				case "start":
					loc.buts.start	.style.display = "block";
					loc.buts.stop	.style.display = "none";
					loc.buts.downl	.style.display = "block";
					break;
				case "stop":
					loc.buts.start	.style.display = "none";
					loc.buts.stop	.style.display = "block";
					loc.buts.downl	.style.display = "block";
					break;
				}
			}
				loc.SH("start");
		// GUI in
			loc.buts.start.onclick = function(e){
				loc.startRec();
			}
			loc.buts.stop.onclick = function(e){
				loc.stopRec();
			}
			loc.buts.downl.onclick = function(e){
				if(loc.blob == null){
					alert("No recording data!");
					return;
				}
				loc.buts.downl.disabled = true;
				loc.getMP3(function(blob){
					loc.buts.downl.disabled = false;
					if(blob == null)
						return;
					loc.downl.href = w.URL.createObjectURL(blob);
					loc.downl.click();
				});
			}
			loc.downl = newEl(loc.div, "a");
				loc.downl.style.display = "none";
				loc.downl.download = "sound";
		// system
			loc.startRec = function(){
				loc.drvData = [];
				loc.drvRec.start();
				loc.SH("stop");
			}
			loc.stopRec = function(){
				loc.drvRec.stop();
				loc.SH("start");
			}
			loc.ctx = obj.ctx.createGain();
			loc.ctxRec = obj.ctx.createMediaStreamDestination();
				loc.ctx.connect(loc.ctxRec);
			loc.drvRec = new MediaRecorder(loc.ctxRec.stream);
			loc.drvRec.ondataavailable = function(e){
				loc.drvData.push(e.data);
			}
			loc.getBlob = function(){
				return new Blob(loc.drvData, {type: "audio:ogg;codecs:opus"});
			}
			loc.drvRec.onstop = function(e){
				var blob = loc.getBlob();
				loc.blob = blob;
				var src = URL.createObjectURL(blob);
				loc.aud.src = src;
			}
			loc.getMP3 = function(f){
				if(w.Mp3LameEncoder == undefined){
					alert("No find MP3 codec!");
					f(null);
					return;
				}
				var file = new FileReader();
				function audioBuffer2MP3blob(audioBuffer){
					var buffers = [
						audioBuffer.getChannelData(0),
						audioBuffer.getChannelData(1)
					];
					var sampleRate = audioBuffer.sampleRate;
					var bit = loc.bitRateMP3;
					var mp3 = new Mp3LameEncoder(sampleRate, bit);
					var i;
					for(i = 0; i < buffers[0].length; i+= sampleRate){
						console.log(Math.floor(i / buffers[0].length * 100) + "%");
						mp3.encode([
							buffers[0].slice(i, i + sampleRate),
							buffers[1].slice(i, i + sampleRate)
						]);
					}
					var blob = mp3.finish();
					console.log("Sucs!");
					return blob;
				}
				file.onload = function(e){
					var arrB = file.result;
					obj.ctx.decodeAudioData(arrB).
						then(function(e){
							var blob = null;
							try{
								blob = audioBuffer2MP3blob(e);
							}catch(err){
								alert("Do not create MP3 from audio");
								console.error(err);
							}
							f(blob);
						}).catch(function(e){
							alert("Do not create audio from bin data");
							console.log(e);
							f(null);
						});
				}
				file.onerror = function(e){
					alert("Do not create bin data from blob");
					console.error(e);
					f(null);
				}
				file.readAsArrayBuffer(loc.blob);
			}
			;(function(){
				var ctx = obj.ctx.createMediaElementSource(loc.aud);
				ctx.connect(loc.ctx);
			})();
		// Inners & Outers
			loc.inners = [obj.objs.innerS(loc.ctx)];
				glb.getInners = function(){return loc.inners;}
			loc.outers = [obj.objs.outerS(loc.ctx)];
				glb.getOuters = function(){return loc.outers;}
		// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "recorder";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "recorder")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- SPECTR
		obj.objs.spectr = function(){
			var glb = obj.objs.graph();
			var loc = {};
			glb.setCaption("SPECTR");
			// system
			loc.ctx = obj.ctx.createAnalyser();
			loc.ctx.fftSize = 1024; // len
			loc.max = 256; // low
			loc.len = loc.ctx.frequencyBinCount;
			loc.data = new Uint8Array(loc.len);
			// GUI out
			loc.gr = glb.getGr();
				glb.getGr = undefined;
			glb.setDraw(function(){
				loc.ctx.getByteFrequencyData(loc.data);
				loc.gr.canvas.width += 0;
				loc.gr.strokeStyle = glb.getStyle(0);
				loc.gr.beginPath();
				var kx = function(x){
					return x * loc.gr.canvas.width / loc.len;
				}
				var ky = function(y){
					return loc.gr.canvas.height * (1 - y / loc.max);
				}
				var i;
				for(i = 0; i < loc.len; i++){
					if(i == 0)
						loc.gr.moveTo(kx(i), ky(loc.data[i]));
					else
						loc.gr.lineTo(kx(i), ky(loc.data[i]));
				}
				loc.gr.stroke();
			});
				loc.setDraw = glb.setDraw;
				glb.setDraw = undefined;
			// inners
			loc.inners = [obj.objs.innerS(loc.ctx)];
			glb.getInners = function(){return loc.inners;}
			// outers
			loc.outers = [obj.objs.outerS(loc.ctx)];
			glb.getOuters = function(){return loc.outers;}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.back = loc.backSaver.getState();
				res.name = "spectr";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "spectr")
					throw "Wrong type state";
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			glb.start();
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- DRUMS
		obj.objs.drums = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			glb.setCaption("DRUMS");
			// Inners & Outers
			loc.inners = [];
			loc.outers = [obj.objs.outerE()];
			glb.getInners = function(){return loc.inners;}
			glb.getOuters = function(){return loc.outers;}
			// GUI
			loc.butArr = [];
			loc.sizeBut = [15, 15]; // Добавить set/get
			loc.setVBut = function(but, state){ // Пока так
				switch(state){
				case 0:
					but.style.opacity = 0.3;
					break
				case 1:
					but.style.opacity = 0.8;
					break;
				case 2:
					but.style.opacity = 0.5;
					break;
				case -1:
					but.style.opacity = 1;
					break;
				}
			}
			loc.reGUI = function(){
				function newF(a){
					return function(e){
						if(loc.arr[a][0] > 0){
							loc.arr[a][0] = 0;
						} else {
							if(e.button == 0){
								loc.arr[a][0] = 1;
							} else {
								loc.arr[a][0] = 2;
							}
						}
						loc.setVBut(e.target, loc.arr[a][0]);
					}
				}
				loc.div.innerHTML = "";
				loc.div.style.width = (loc.arr.length * (loc.sizeBut[0] + 6) + 6) + "px";
				loc.butArr = [];
				var but;
				for(var i = 0; i < loc.arr.length; i++){
					but = w.document.createElement("div");
					but.style.width  = loc.sizeBut[0] + "px";
					but.style.height = loc.sizeBut[1] + "px";
						but.style.background = "rgb(0,0,255)"; // Пока так
						but.style.display = "inline-block";
						but.style.verticalAlign = "top";
						but.style.margin = "3px";
					but.oncontextmenu = function(){return false;};
					but.onmouseup = newF(i);
					loc.setVBut(but, loc.arr[i][0]);
					loc.div.appendChild(but);
					loc.butArr.push(but);
				}
			}
			// system
			loc.arr = [];
			glb.setT = function(val){
				while(loc.arr.length < val*loc.countT){
					loc.arr.push([0]);
				}
				while(loc.arr.length > val*loc.countT){
					loc.arr.pop();
				}
				loc.reGUI();
			}
			glb.getT = function(){return loc.arr.length / loc.countT;}
			loc.countT = 1;
			glb.setCountT = function(val){
				loc.countT = val;
				loc.reGUI();
			}
			glb.getCountT = function(){return loc.countT;}
			glb.setCountT(4);
			glb.setT(4);
			loc.delta = 0; // Add set/get
			loc.lastTik = -1;
			loc.tik = function(){
				var t = obj.ctx.currentTime;
				t = antiTrunc(t * obj.bpm / 60 / loc.countT); // secs -> beat
				t = Math.floor(t * loc.arr.length); // beat -> position
				if(t == loc.lastTik)
					return;
				loc.lastTik = t;
				switch(loc.arr[t][0]){
				case 0:
					break;
				case 1:
					loc.outers[0].run({t: loc.delta});
					break;
				case 2:
					loc.outers[0].run();
					break;
				}
				var tt = (t + loc.arr.length - 1) % loc.arr.length;
				loc.setVBut(loc.butArr[tt], loc.arr[tt][0]);
				loc.setVBut(loc.butArr[t], -1);
			}
			loc.timer = w.setInterval(loc.tik, 10);
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.obj.loc.arr = loc.arr;
				res.obj.glb.countT = glb.getCountT();
				res.back = loc.backSaver.getState();
				res.name = "drums";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "drums")
					throw "Wrong type state";
				glb.setCountT(res.obj.glb.countT);
				glb.setT(res.obj.loc.arr.length / glb.getCountT());
				loc.arr = res.obj.loc.arr;
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			// END
			return filterObj(glb);
		}
		// ------------------------------------------------------------------------------------------- UNIPAR
		/*
		ctx.inner - входной ctx
		ctx.outer - выходной ctx
		arrAP[i].f - преобразователь [-1; 1] => нужный интервал для AudioParam
		arrAP[i].title - название параметра
		arrAP[i].ap - AudioParam
		name - название плагина
		pars.n - количество кусков на цикл
		pars.bpc - бит в цикл
		*/
		obj.objs.unipar = function(ctx, arrAP, name, pars){
			if(pars === undefined){
				pars = {};
			}
			pars.n = pars.n === undefined ? 10 : pars.n;
			pars.bpc = pars.bpc === undefined ? 1 : pars.bpc;
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			// system
			loc.ctx = ctx;
			loc.inners = [obj.objs.innerS(loc.ctx.inner)];
				glb.getInners = function(){return loc.inners;}
			loc.outers = [obj.objs.outerS(loc.ctx.outer)];
				glb.getOuters = function(){return loc.outers;}
			loc.arr = [];
			var el;
			for(var i = 0; i < arrAP.length; i++){
				loc.arr.push((function(){
					var el = {};
					el.f = arrAP[i].f === undefined ? function(v){return v;} : arrAP[i].f;
					el.title = arrAP[i].title === undefined ? "<>" : arrAP[i].title;
					el.pos = 0;
					el.range = newRange(loc.div, -50, 50, 0,
						function(v){
							el.pos = v / 50;
						},
						function(v){
							return el.title + " => " + v;
						}
					);
					el.ff = function(v){
						v += el.pos;
						if(v < -1) v = -1;
						if(v > 1) v = 1;
						return el.f(v);
					}
					el.inner = obj.objs.innerF();
						loc.inners.push(el.inner);
					el.drv = drvAudioParam(
						arrAP[i].ap,
						el.inner,
						10, el.ff, pars
					);
					return el;
				})());
			}
			glb.start = function(){
				for(var i = 0; i < loc.arr.length; i++){
					loc.arr[i].range[1](loc.arr[i].pos);
					loc.arr[i].drv.start();
				}
			}
			glb.stop = function(){
				for(var i = 0; i < loc.arrDrv.length; i++){
					loc.arrDrv[i].stop();
				}
			}
			// Saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.obj.loc.arr = [];
				for(var i = 0; i < loc.arrDrv.length; i++){
					res.obj.loc.arr.push(loc.arrDrv[i].pos);
				}
				res.back = loc.backSaver.getState();
				res.name = name;
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != name)
					throw "Wrong type state";
				
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- VOLUME & PAN
		obj.objs.volpan = function(){
			var loc = {};
			loc.ctxVol = obj.ctx.createGain();
			loc.ctxPan = obj.ctx.createStereoPanner();
			loc.ctxVol.connect(loc.ctxPan);
			var glb = obj.objs.unipar(
				{inner: loc.ctxVol, outer: loc.ctxPan},
				[
					{ap: loc.ctxVol.gain, f: function(v){return v+1;}, title: "Gain"},
					{ap: loc.ctxPan.pan, title: "Panner"}
				],
				"volpan"
			);
			glb.setCaption("Volumer & Panner");
			glb.start();
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- TESTER
		obj.objs.tester = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.getInners = loc.getOuters = function(){return [];}
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			// Default questions
			loc.testDef = {};
			loc.testDef.name = "Кто ты из группы Иртегова?";
			loc.testDef.pArr = [
				"Стас", "Ася", "Настя", "Дима Селезнев", "Савелий", "Ира", "Никита", "Дима Юрганов", "Саша", "Иртегов!", "Ты кто вообще"
			//	0,      1,     2,       3,               4,         5,     6,        7,              8,      9,          10
			];
			loc.testDef.qArr = [
				{
					q: "Ходите ли вы на пары?",
					a: ["да", "нет"],
					p: [[0,1,2,3,5,6,7,8,9], [4]]
				}, {
					q: "Насколько хорошо вы знаете английский?",
					a: ["Ну вы отойдите, я сейчас погуглю", "Yeah", "Net"],
					p: [[0,1,4,5,7,8], [3,2,9], [6]]
				}, {
					q: "Знаете ли вы пароль от терминалки?",
					a: ["да", "не очень"],
					p: [[0,1,2,3,4,5,6,8,9], [7]]
				}, {
					q: "Сколько максимум семинаров вы сдавали одну лабу по осям?",
					a: ["Не более одного", "Одну-две", "Три-четыре", "Да сколько можно сдавать-то"],
					p: [[9], [3], [1,4,6,7,0], [2,5,8]]
				}, {
					q: "Конфликтный ли вы человек?",
					a: ["да", "нет", "средненько"],
					p: [[3,9], [1,5,6], [0,2,4,7,8]]
				}, {
					q: "Добиваетесь ли вы своих целей?",
					a: ["Почти всегда", "Пытаюсь, но как-то не оч", "Всегда"],
					p: [[3], [0,1,2,4,5,6,7,8], [9]]
				}, {
					q: "Есть ли у вас проблемы с формулировкой фраз?",
					a: ["Нет", "Есть справка"],
					p: [[0,1,2,3,4,5,7,8,9], [6]]
				}, {
					q: "Знаете ли вы, что такое указатель?",
					a: ["Да, но меня должны были отчислить на первом курсе", "Нет, но меня еще не отчислили", "В языке Си?"],
					p: [[0,2,3,5,7,8], [1,6], [9]]
				}, {
					q: "Насколько хорошо вы знаете, то такое ядро?",
					a: ["Программа", "Часть ОС", "Ну вы отойдите, я сейчас погуглю", "Ну я же не должен вам все объяснять"],
					p: [[6,1,5], [8,3], [0,2,4,6,7], [9]]
				}, {
					q: "Что такое метод?",
					a: ["Это что-то, что отлаживает программист", "Это часть класса", "...", "Программа"],
					p: [[6], [0,1,2,3,4,5,7,8], [], [9]]
				}, {
					q: "Why is IETF привелигерована?",
					a: ["a"],
					p: [[]]
				}, {
					q: "Чем занимается программист большую часть своего времени?",
					a: ["Пишет программы", "Отлаживает программы", "Ну нет, ну это не про то"],
					p: [[0,1,2,3,4,5,7,8], [6], [9]]
				}, {
					q: "Как вы думаете, на каком предете была раскрыта тема setuid?",
					a: ["Что?"],
					p: [[]]
				}, {
					q: "АЛЕ?",
					a: ["АЛЕЕЕЕ!", "*нервно вздрогнул*", "..."],
					p: [[1,6,9], [1,5], [0,2,3,4,7,8]]
				}, {
					q: "Любите ли вы кофе с пончиками из Кузины?",
					a: ["Да", "Нет"],
					p: [[1,9], [0,2,3,4,5,6,7,8]]
				}
			];
			loc.cbDef = function(res){
				var max = 0;
				for(var i = 1; i < res.length; i++){
					if(res[i] > res[max]){
						max = i;
					}
				}
				alert("Вы весьма похожи на человека по имени " + this.pArr[max]);
			}
			loc.test = {
				qArr: []
			};
			// GUI in
			loc.goTest = function(){ // Очередной вопрос
				if(loc.test.n > 0){ // Если вопрос не первый
					var q = loc.guiEls.range[1](); // Позиция бегунка
					var p = q[0].p[q[1]]; // Люди, выбравшие этот вариант
					for(var i = 0; i < p.length; i++){
						loc.test.res[p[i]]++; // Для каждого увеличиваем его позицию
					}
				}
				if(loc.test.n == loc.test.qArr.length){ // Если был последний
					loc.guiEls.but.onclick = function(){ // Ставим заглушку
						alert("Спасибо за прохождение!");
					}
					loc.guiEls.range[0].style.display = "none"; // Скрываем бегунок
					loc.guiEls.q.innerHTML = "Спасибо за прохождение!"; // Ставим надпись
					loc.test.cb(loc.test.res); // Вызываем колбэк
					return;
				}
				var q = loc.test.qArr[loc.test.n]; // Следующий вопрос
				q.map = []; // Создаем стахостику
				for(var i = 0; i < q.a.length; i++){
					q.map.push(i);
				}
				q.map.sort(function(){return Math.random() - 0.5;}); // Перемешиваем
				loc.test.n++; // Передвигаем итератор
				loc.guiEls.q.innerHTML = q.q;
				loc.guiEls.range[1](0); // Устанавливаем range
			}
			glb.goTest = function(test, cb){ // Начать тест
				loc.test = JSON.parse(JSON.stringify(test)); // Копируем
				loc.test.qArr.sort(function(){return Math.random() - 0.5;}); // Перемешиваем вопросы
				loc.test.res = []; // Результат
				for(var i = 0; i < loc.test.qArr.length; i++){
					loc.test.res.push(0); // Заполняем нулями
				}
				loc.test.cb = cb; // Колбэк
				loc.test.n = 0; // Начинаем с первого
				glb.setCaption("Тест => " + test.name); // Устанавливаем имя
				loc.guiEls.but.onclick = loc.goTest;
				loc.guiEls.range[0].style.display = "";
				loc.goTest(); // Очередной вопрос
			}
			// GUI out
			loc.guiEls = {}; // Элементы ГУИ
			loc.guiEls.fV2A = function(v){ // Позицию range в вариант ответа
				if(loc.test.qArr.length == 0)
					return [{a: ["<Загрузка...>"]}, 0];
				var q = loc.test.qArr[loc.test.n-1]; // Текущий вопрос
				v = Math.floor(v * q.map.length / 100); // Позиция в массиве
				return [q, q.map[v]]; // Нужный вариант
			}
			loc.guiEls.q = newEl(loc.div, "div"); // Сам вопрос
				loc.guiEls.q.style.fontSize = "20px";
				loc.guiEls.q.innerHTML = "Загрузка...";
			loc.guiEls.range = newRange(loc.div, 0, 99, 0, // Варианты ответов
				function(v){
					return loc.guiEls.fV2A(v);
				},
				function(v){
					var q = loc.guiEls.fV2A(v);
					return q[0].a[q[1]];
				}
			);
			loc.guiEls.but = newEl(loc.div, "button"); // Кнопка "Дальше"
				loc.guiEls.but.innerHTML = "NEXT";
			// End
			glb.goTest(loc.testDef, loc.cbDef);
			return filterObj(glb);
		}
		// -------------------------------------------------------------------------------------------------------- DRUMMACHINE
		obj.objs.drummachine = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			glb.setCaption("Drum Machine");
			// System
			loc.patcher = obj.objs.patcher();
				loc.patcher.setWState(false);
				loc.patcher.setPosXDevice(0, loc.patcher.getSizeSQ() * 3 + 20 * 4);
				loc.patcher.setPosYDevice(0, 20);
				glb.getInners = loc.patcher.getInners;
				glb.getOuters = loc.patcher.getOuters;
			loc.arr = [];
			glb.addDrumLine = function(){
				var i;
				for(i = 0; i < loc.arr.length; i++){
					if(loc.arr[i] == undefined || loc.patcher.getDevice(loc.arr[i].link) == null){
						glb.remDrumLine(i);
						break;
					}
				}
				var el = {};
				var bufObj;
				var bufTarget = swapTarget(loc.div);
					el.drum = obj.objs.drums();
				swapTarget(bufTarget);
					el.drum.setPosX(100);
					el.drum.setPosY(20 + 50 * i);
				el.link = loc.patcher.addDevice("joinE");
					bufObj = loc.patcher.getDevice(el.link);
					bufObj.setCaption("Drum driver");
					bufObj.setInners(1);
					el.drum.getOuters()[0].connect(
						bufObj.getInners()[0]
					);
					loc.patcher.setPosXDevice(el.link, 20 + 0 * (loc.patcher.getSizeSQ() + 20));
					loc.patcher.setPosYDevice(el.link, 20 + i * (loc.patcher.getSizeSQ() + 20));
					loc.patcher.setWStateDevice(el.link, false);
				el.data = loc.patcher.addDevice("file");
					bufObj = loc.patcher.getDevice(el.data);
					bufObj.setLoop(false);
					//bufObj.setCaption("File");
					loc.patcher.setPosXDevice(el.data, 20 + 1 * (loc.patcher.getSizeSQ() + 20));
					loc.patcher.setPosYDevice(el.data, 20 + i * (loc.patcher.getSizeSQ() + 20));
					loc.patcher.setWStateDevice(el.data, false);
				el.vol  = loc.patcher.addDevice("volpan");
					bufObj = loc.patcher.getDevice(el.vol);
					bufObj.setCaption("Drum vol & pan");
					loc.patcher.setPosXDevice(el.vol,  20 + 2 * (loc.patcher.getSizeSQ() + 20));
					loc.patcher.setPosYDevice(el.vol,  20 + i * (loc.patcher.getSizeSQ() + 20));
					loc.patcher.setWStateDevice(el.vol,  false);
				loc.patcher.connect(el.link, el.data, 0, 0);
				loc.patcher.connect(el.data, el.vol,  0, 0);
					bufObj = loc.patcher.getDevice(0);
					var count = bufObj.getInners().length;
					bufObj.setInners(count + 1);
				loc.patcher.connect(el.vol, 0,        0, count);
				loc.arr[i] = el;
			}
			glb.remDrumLine = function(ind){
				var el = loc.arr[ind];
				if(el == undefined)
					return;
				loc.patcher.remDevice(el.link);
				loc.patcher.remDevice(el.file);
				loc.patcher.remDevice(el.vol);
				el.drum.setWState(null);
				loc.arr[ind] = undefined;
			}
			// Добавить Saver надо еще, но я хочу спать
			// END
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- EQ
		obj.objs.eq = function(){
			var glb = obj.objs.window();
			var loc = {};
			loc.div = glb.getContainer();
				glb.getContainer = undefined;
			glb.setCaption("EQ");
			// inners & outers
			loc.IO = {
				inner: obj.ctx.createGain(),
				outer: obj.ctx.createGain()
			}
			loc.inners = [
				obj.objs.innerS(loc.IO.inner)
			];
			loc.outers = [
				obj.objs.outerS(loc.IO.outer)
			];
			glb.getInners = function(){return loc.inners;}
			glb.getOuters = function(){return loc.outers;}
			// system
			loc.pars = {};
				loc.pars.n = 10;
				loc.pars.bpc = 1;
			loc.createPart = function(){
				var el = {};
				function param2more(param, f, s){ // AudioParam; [-1, 1] => right; start value in [-1, 1]
					var el = {};
					el.param = param;
					el.v = 0;
					el.ff = function(v){ // for el.drv
						v += el.v;
						if(v < -1) v = -1;
						if(v > 1) v = 1;
						return f(v);
					}
					el.rangeEvent = function(v){ // for el.range
						el.v = v*2-1;
					}
					el.range = newCircleRange(null, el.rangeEvent);
					el.value = function(v){
						if(v !== undefined){
							el.v = v;
							el.range.value((el.v+1)/2);
						}
						return el.v;
					}
					el.inner = obj.objs.innerF();
					el.drv = drvAudioParam(
						el.param, el.inner, 10, el.ff, loc.pars
					);
					el.value(s);
					el.drv.start();
					return {
						range: el.range.target,
						value: el.value,
						inner: el.inner
					};
				}
				el.ctx = obj.ctx.createBiquadFilter();
				el.params = {
					freq: param2more(
						el.ctx.frequency,
						function(v){
							return 220 * Math.pow(2, v * 5.5);
						}, 0
					),
					Q: param2more(
						el.ctx.Q,
						function(v){
							return Math.pow(10, v);
						}, 0
					),
					gain: param2more(
						el.ctx.gain,
						function(v){
							return 40 * v;
						}, 0
					)
				};
				return el;
			}
			loc.ctx = loc.createPart();
			loc.IO.inner.connect(loc.ctx.ctx);
			loc.ctx.ctx.connect(loc.IO.outer);
			loc.div.appendChild(loc.ctx.params.freq.range);
			loc.div.appendChild(loc.ctx.params.Q   .range);
			loc.div.appendChild(loc.ctx.params.gain.range);
			loc.inners.push(loc.ctx.params.freq.inner);
			loc.inners.push(loc.ctx.params.Q   .inner);
			loc.inners.push(loc.ctx.params.gain.inner);
			// end
			return filterObj(glb);
		}
		// ----------------------------------------------------------------------------------------------------- TIMEMARKER
		obj.objs.timeMarker = function(){
			var glb = obj.objs.window();
			var loc = {};
			// ------------- inners & outers
			loc.inners = [];
			loc.outers = [
				obj.objs.outerE(),
				obj.objs.outerE()
			];
			glb.getInners = function(){return loc.inners;}
			glb.getOuters = function(){return loc.outers;}
			// ------------ GUI out
			loc.div = glb.getContainer();
				delete glb.getContainer;
			glb.setCaption("Time Marker");
			loc.startBut = newEl(loc.div, "button");
			loc.stopBut  = newEl(loc.div, "button");
			loc.textDiv  = newEl(loc.div, "div");
			loc.quantBut = newEl(loc.textDiv, "button");
			loc.playBut  = newEl(loc.textDiv, "button");
			loc.textArea = newEl(loc.textDiv, "textarea");
			loc.startBut.innerHTML = "Start record";
			loc.stopBut .innerHTML = "Stop record";
			loc.quantBut.innerHTML = "Quantize";
			loc.playBut .innerHTML = "Play/Stop";
			loc.SH = function(pos){
				switch(pos){
				case "rec":
					loc.startBut.style.display = "none";
					loc.stopBut .style.display = "";
					loc.textDiv .style.display = "none";
					break;
				case "wait":
					loc.startBut.style.display = "";
					loc.stopBut .style.display = "none";
					loc.textDiv .style.display = "";
					break;
				}
			}
				loc.SH("wait");
			// ------------ system
			loc.delay = 20;
			loc.recTime = [];
			loc.getTime = function(){
				//return (new Date()).getTime();
				return obj.ctx.currentTime*1000;
			}
			loc.start = function(){
				loc.recTime = [];
				var startTime = loc.getTime();
				w.onkeydown = function(e){
					loc.recTime.push({
						k: e.keyCode,
						t: loc.getTime() - startTime
					});
					loc.outers[1].run({t: 0, k: e.keyCode});
				}
				loc.outers[0].run({t: 0});
			}
			loc.stop = function(){
				w.onkeydown = null;
				loc.outers[0].run();
			}
			loc.timer = null;
			loc.playstop = function(){
				if(loc.timer !== null){
					loc.outers[0].run();
					loc.outers[1].run();
					w.clearInterval(loc.timer);
					loc.timer = null;
				} else {
					if(loc.recTime.length == 0){
						alert("You are empty!");
						return;
					}
					var startTime = loc.getTime();
					var pos = 0;
					loc.timer = w.setInterval(function(){
						var nowTime = loc.getTime() - startTime;
						if(nowTime < loc.recTime[pos].t){
							return;
						}
						loc.outers[1].run({t: 0, k: loc.recTime[pos].k});
						pos++;
						if(pos >= loc.recTime.length){
							loc.playstop();
						}
					}, 1);
					loc.outers[0].run({t: 0});
					loc.startTime = loc.getTime();
				}
			}
			loc.quantize = function(){
				var bpm = +prompt("Enter the bpm", "120");
				var eps = +prompt("Enter the eps", "1");
				var del = +prompt("Enter the delay", "0");
				if(bpm != bpm || eps != eps || del != del){
					return;
				}
				function quant(del){
					//var old = del;
					del *= bpm * eps / 60 / 1000;
					del = Math.floor(del + 0.001);
					del *= 60 * 1000 / bpm / eps;
					//console.log(old - del);
					return del;
				}
				for(var i = 0; i < loc.recTime.length-1; i++){
					loc.recTime[i+1].t = loc.recTime[i].t + quant(
							loc.recTime[i+1].t -
							loc.recTime[i].t
					);
				}
				for(var i = 0; i < loc.recTime.length; i++){
					loc.recTime[i].t -= del;
				}
			}
			// ----------------- GUI in
			loc.startBut.onclick = function(e){
				loc.start();
				loc.SH("rec");
			}
			loc.stopBut .onclick = function(e){
				loc.stop();
				loc.textArea.value = loc.recTime.map(el => el.t).join("\n");
				loc.SH("wait");
			}
			loc.playBut .onclick = function(e){
				loc.playstop();
			}
			loc.quantBut.onclick = function(e){
				loc.quantize();
				loc.textArea.value = loc.recTime.map(el => el.t).join("\n");
			}
			// -------------------- saver
			loc.backSaver = glb.getSaver();
			loc.saver = {};
			loc.saver.getState = function(){
				var res = {};
				res.obj = {};
				res.obj.glb = {};
				res.obj.loc = {};
				res.obj.loc.recTime = loc.recTime;
				res.back = loc.backSaver.getState();
				res.name = "timeMarker";
				return res;
			}
			loc.saver.setState = function(res){
				if(res.name != "timeMarker")
					throw "Wrong type state";
				loc.recTime = res.obj.loc.recTime;
				loc.textArea.value = loc.recTime.map(el => el.t).join("\n");
				loc.backSaver.setState(res.back);
			}
			glb.getSaver = function(){return loc.saver;}
			return glb;
		}
		// ----------------------------------------------------------------------------------------------------- END
		obj.out = obj.objs.out();
			obj.objs.out = undefined;
			obj.objs = filterObj(obj.objs);
		obj.log = function(a, b){
			function strlen(str, len){
				while(str.length < len)
					str = " " + str;
				return str;
			}
			if(a == undefined){
				obj.out.setLog("");
				return;
			}
			if(a == "note"){
				b = +b;
				if(b != b)
					throw "wrong logging";
				while(b < 0)
					b += 12;
				while(b >= 12)
					b -= 12;
				var str = [
					"A", "A#",
					"B",
					"C", "C#",
					"D", "D#",
					"E",
					"F", "F#",
					"G", "G#"
				];
				obj.out.setLog("Note " + str[b]);
				return;
			}
			if(a == "point"){
				var p = b[0];
				var t = b[1];
				var res = [p[0], p[1]];
				if(t[0] > 0)
					res[0] = Math.floor(p[0]*t[0]) + "/" + t[0];
				else
					res[0] = "<br>" + res[0];
				if(t[1] > 0)
					res[1] = Math.floor(p[1]*t[1]) + "/" + t[1];
				else
					res[1] = "<br>" + res[1];
				obj.out.setLog('<div style="width:300px;text-align:center">Point<br>' + res[0] + " " + res[1] + "</div>");
				return;
			}
			if(a == "log"){
				obj.out.setLog("<i>" + b + "</i>");
				return;
			}
		}
		return obj;
	})();
}